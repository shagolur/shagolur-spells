package fr.shagolur.spells;

import fr.shagolur.core.ShagolurModule;
import fr.shagolur.spells.classes.ClassesManager;
import fr.shagolur.spells.commands.DebugExtension;
import fr.shagolur.spells.commands.ShagolurSpellsCommand;
import fr.shagolur.spells.listeners.InternalListeners;
import fr.shagolur.spells.listeners.PlayerInteractListeners;
import fr.shagolur.spells.spells.SpellsManager;
import org.bukkit.Bukkit;
import org.jetbrains.annotations.NotNull;

public class ShagolurSpells extends ShagolurModule {
	
	public static ShagolurSpells module() {
		return SPELLS;
	}
	
	private static ShagolurSpells SPELLS;
	
	private SpellsManager spellsManager;
	private ClassesManager classesManager;
	
	@Override
	public void onEnable() {
		SPELLS = this;
		// Init components
		classesManager = new ClassesManager();
		spellsManager = new SpellsManager();
		// Init listeners
		Bukkit.getPluginManager().registerEvents(new InternalListeners(), this);
		Bukkit.getPluginManager().registerEvents(new PlayerInteractListeners(), this);
		// Init commands
		new ShagolurSpellsCommand("sg.spells");
		// Command extensions
		new DebugExtension();
	}
	
	@Override
	public void onDisable() {
		spellsManager.purge();
	}
	
	public static @NotNull SpellsManager getSpellsManager() {
		return SPELLS.spellsManager;
	}
	public static @NotNull ClassesManager getClassesManager() {
		return SPELLS.classesManager;
	}
}
