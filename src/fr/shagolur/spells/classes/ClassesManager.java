package fr.shagolur.spells.classes;

import fr.shagolur.core.storage.PathProvider;
import fr.shagolur.core.storage.StorableManager;
import fr.shagolur.spells.storage.ClassesPathProvider;

public class ClassesManager extends StorableManager<SClass> {
	
	@Override
	protected PathProvider<SClass> getProvider() {
		return new ClassesPathProvider();
	}
	
	@Override
	protected String getStoredTypeName() {
		return "classes";
	}
	
}
