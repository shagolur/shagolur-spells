package fr.shagolur.spells.classes;

import fr.shagolur.core.storage.Serializer;
import fr.shagolur.core.storage.Storable;
import org.bukkit.ChatColor;

public class SClass implements Storable {
	
	private final String identifier;
	private final String name;
	
	public SClass(String identifier, String name) {
		this.identifier = identifier;
		this.name = ChatColor.translateAlternateColorCodes('&', name);
	}
	
	public String getDisplayName() {
		return name;
	}
	
	@Override
	public Object storageID() {
		return identifier;
	}
	
	@Override
	public Serializer<?> serialize() {
		throw new RuntimeException("A ShagolurClass cannot be serialized to server from plugin.");
	}
	
	@Override
	public boolean alreadyInBDD() {return true;}
	
	@Override
	public void declarePersistentInBDD() {}
	
}
