package fr.shagolur.spells.commands;

import fr.shagolur.core.Shagolur;
import fr.shagolur.core.commands.CommandSenderHandler;
import fr.shagolur.spells.ShagolurSpells;
import fr.shagolur.spells.parser.statements.Statement;
import fr.shagolur.spells.spells.Spell;
import org.bukkit.command.CommandSender;

public class DebugExtension implements CommandSenderHandler {
	
	public DebugExtension() {
		Shagolur.getDebugCommand().addDataExtension(
				"spell",
				() -> ShagolurSpells.getSpellsManager().getStream()
						.map(Spell::getID)
						.toList(),
				this::handleCommand
		);
	}
	
	public void handleCommand(String spellId, CommandSender sender) {
		Spell spell = ShagolurSpells.getSpellsManager().get(spellId);
		if(spell == null) {
			sendError(sender, "Spell inconnu : \""+spellId+"\".");
			return;
		}
		sendSuccess(sender, "------- [SPELL DATA {"+spell.getID()+"§a}] -------");
		sendInfo(sender, "Nom : " + spell.getName());
		sendInfo(sender, "Mana cost : §b" + spell.getManaCost() + "§e, cooldown : §a" + spell.getCooldown() + "§es.");
		sendInfo(sender, "Statements:");
		for(Statement s : spell.getParts())
			sendInfo(sender, "§2- §f"+s);
		sendInfo(sender, "------------------------------------------");
	}
	
}
