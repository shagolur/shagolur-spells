package fr.shagolur.spells.commands;

import fr.shagolur.core.Shagolur;
import fr.shagolur.core.commands.JamArg;
import fr.shagolur.core.commands.JamCommand;
import fr.shagolur.core.players.SPlayer;
import fr.shagolur.spells.ShagolurSpells;
import fr.shagolur.spells.spells.Spell;
import fr.shagolur.spells.ui.GiveSpellsUI;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

public class ShagolurSpellsCommand extends JamCommand {
	
	public ShagolurSpellsCommand(String command) {
		super(command, ShagolurSpells.module());
		addArgument(0, "give", "ui", "reload");
		addArgumentIf(
				1,
				new JamArg[] {new JamArg(0, "give")},
				() -> ShagolurSpells.getSpellsManager()
						.getStream()
						.filter(Spell::isValid)
						.map(Spell::getID)
						.toList()
		);
	}
	
	@Override
	public void onCommand(@NotNull CommandSender sender, @NotNull String label, @NotNull String[] args) {
		if(args.length == 0 || args[0].equals("ui")) {
			if(!(sender instanceof Player)) {
				reject(sender, false, label);
				return;
			}
			new GiveSpellsUI((Player)sender);
			return;
		}
		if(args[0].equals("reload")) {
			ShagolurSpells.getSpellsManager().reloadFromBase();
			sendSuccess(sender, "Spells from database reloaded.");
			return;
		}
		if(args[0].equals("give")) {
			if(!(sender instanceof Player)) {
				reject(sender, false, label);
				return;
			}
			SPlayer player = Shagolur.getPlayersManager().getPlayer((Player)sender);
			if(args.length < 2) {
				player.sendError("Il faut spécifier le spell à donner !");
				return;
			}
			String id = args[1];
			Spell spell = ShagolurSpells.getSpellsManager().get(id);
			if(spell == null) {
				player.sendError("L'id \""+id+"\" ne correspond à aucun spell.");
				return;
			}
			player.getPlayer().getInventory().addItem(spell.toItemStack());
			player.sendSuccess("Spell bien reçu.");
			return;
		}
		sendError(sender, "Sous-commande inconnue. Valeurs acceptées : [ui, give, reload].");
	}
	
	@Override
	public CommandExecutors getAllowedExecutors() {
		return CommandExecutors.ALL;
	}
	
}
