package fr.shagolur.spells.listeners;

import fr.shagolur.core.events.ShagolurStorageReloadEvent;
import fr.shagolur.spells.ShagolurSpells;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class InternalListeners implements Listener {
	
	@EventHandler
	public void shagolurReload(ShagolurStorageReloadEvent e) {
		ShagolurSpells.getClassesManager().reloadFromBase();
		ShagolurSpells.getSpellsManager().reloadFromBase();
	}
	
}
