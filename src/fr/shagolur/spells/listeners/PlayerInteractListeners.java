package fr.shagolur.spells.listeners;

import fr.shagolur.spells.ShagolurSpells;
import fr.shagolur.spells.spells.Spell;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class PlayerInteractListeners implements Listener {

	@EventHandler
	public void playerRightClick(PlayerInteractEvent e) {
		if(!e.getAction().isRightClick())
			return;
		ItemStack is = e.getPlayer().getInventory().getItemInMainHand();
		Spell spell = ShagolurSpells.getSpellsManager().getFromItem(is);
		if(spell != null) {
			e.setCancelled(true);
			spell.tryToCast(e.getPlayer());
		}
	}

}
