package fr.shagolur.spells.parser;

import fr.shagolur.core.Shagolur;
import fr.shagolur.spells.parser.elements.StreamTokens;
import fr.shagolur.spells.parser.elements.StreamChars;
import fr.shagolur.spells.parser.elements.Token;
import fr.shagolur.spells.parser.elements.TokenType;
import fr.shagolur.spells.parser.errors.BadTokenError;
import fr.shagolur.spells.parser.errors.SyntaxError;
import fr.shagolur.spells.parser.statements.Statement;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public final class SpellParser {
	private SpellParser() {}
	
	public static void printStackTraces(Throwable e) {
		e.printStackTrace();
	}
	
	public static boolean isEffectValid(String effect) {
		return Shagolur.getEffectsManager().exists(effect);
	}
	
	public static List<Statement> parse(File file) throws BadTokenError, SyntaxError {
		return parse(new StreamChars(file));
	}
	public static List<Statement> parse(String code) throws BadTokenError, SyntaxError {
		return parse(new StreamChars(code));
	}
	private static List<Statement> parse(StreamChars chars) throws BadTokenError, SyntaxError {
		List<Token> tokensList = Tokenizer.tokenize(chars);
		
		// Create a stream tokens
		StreamTokens tokens = new StreamTokens(tokensList);
		// Build the list of expressions
		List<Statement> statements = new ArrayList<>();
		while(tokens.isNotFuture(TokenType.EOF)) {
			Statement statement = Statement.generateExpression(tokens);
			statements.add(statement);
			
			Shagolur.log("+> " + statement);
		}
		return statements;
	}
	
}
