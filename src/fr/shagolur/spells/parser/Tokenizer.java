package fr.shagolur.spells.parser;

import fr.shagolur.core.Shagolur;
import fr.shagolur.spells.parser.errors.SyntaxError;
import fr.shagolur.spells.parser.elements.StreamChars;
import fr.shagolur.spells.parser.elements.Token;
import fr.shagolur.spells.parser.elements.TokenType;

import java.util.Map;
import java.util.*;


class Tokenizer {
	private final static Map<Character, TokenType> SIMPLE_TOKENS = new HashMap<>();
	static {
		SIMPLE_TOKENS.put(':', TokenType.COLON);
		SIMPLE_TOKENS.put(';', TokenType.SEMICOLON);
		SIMPLE_TOKENS.put('{', TokenType.BLOCK_START);
		SIMPLE_TOKENS.put('}', TokenType.BLOCK_END);
		SIMPLE_TOKENS.put('(', TokenType.PAREN_OPEN);
		SIMPLE_TOKENS.put(')', TokenType.PAREN_CLOSE);
		SIMPLE_TOKENS.put('[', TokenType.ARRAY_START);
		SIMPLE_TOKENS.put(']', TokenType.ARRAY_END);
		SIMPLE_TOKENS.put('=', TokenType.EQUAL);
		SIMPLE_TOKENS.put(',', TokenType.COMMA);
		SIMPLE_TOKENS.put('+', TokenType.OPERATOR_ADD);
		SIMPLE_TOKENS.put('-', TokenType.OPERATOR_SUB);
		SIMPLE_TOKENS.put('*', TokenType.OPERATOR_MUL);
		SIMPLE_TOKENS.put('/', TokenType.OPERATOR_DIV);
	}
	
	private final static Map<String, TokenType> KEYWORD_TOKEN = new HashMap<>();
	static {
		KEYWORD_TOKEN.put("as", TokenType.AS);
		KEYWORD_TOKEN.put("after", TokenType.AFTER);
		KEYWORD_TOKEN.put("to", TokenType.TO);
		KEYWORD_TOKEN.put("at", TokenType.AT);
		KEYWORD_TOKEN.put("do", TokenType.DO);
		KEYWORD_TOKEN.put("in", TokenType.IN);
		KEYWORD_TOKEN.put("around", TokenType.AROUND);
		KEYWORD_TOKEN.put("throw", TokenType.THROW);
		KEYWORD_TOKEN.put("during", TokenType.DURING);
		KEYWORD_TOKEN.put("from", TokenType.FROM);
		KEYWORD_TOKEN.put("of", TokenType.OF);
		KEYWORD_TOKEN.put("with", TokenType.WITH);
		KEYWORD_TOKEN.put("remove", TokenType.REMOVE);
		// CONTROL
		KEYWORD_TOKEN.put("for", TokenType.FOR);
		KEYWORD_TOKEN.put("loop", TokenType.LOOP);
		KEYWORD_TOKEN.put("if", TokenType.IF);
		KEYWORD_TOKEN.put("else", TokenType.ELSE);
		// EXPRESSIONS
		KEYWORD_TOKEN.put("apply", TokenType.APPLY);
		KEYWORD_TOKEN.put("send", TokenType.SEND);
		KEYWORD_TOKEN.put("spawn", TokenType.SPAWN);
		KEYWORD_TOKEN.put("execute", TokenType.EXECUTE);
		KEYWORD_TOKEN.put("damage", TokenType.DAMAGE);
		KEYWORD_TOKEN.put("heal", TokenType.HEAL);
		KEYWORD_TOKEN.put("set", TokenType.SET);
		KEYWORD_TOKEN.put("teleport", TokenType.TELEPORT);
		KEYWORD_TOKEN.put("return", TokenType.RETURN);
		KEYWORD_TOKEN.put("stop", TokenType.STOP);
		KEYWORD_TOKEN.put("break", TokenType.BREAK);
		// METHODS
		KEYWORD_TOKEN.put("loc",        TokenType.METH_LOCATION);
		KEYWORD_TOKEN.put("location",   TokenType.METH_LOCATION);
		KEYWORD_TOKEN.put("dis",        TokenType.METH_DISTANCE);
		KEYWORD_TOKEN.put("distance",   TokenType.METH_DISTANCE);
		KEYWORD_TOKEN.put("raycast",   TokenType.METH_RAYCAST);
		KEYWORD_TOKEN.put("exists",   TokenType.METH_EXISTS);
		KEYWORD_TOKEN.put("get_level",   TokenType.METH_GET_LEVEL);
	}
	
	private final static char[] ALPHA = "ABCDEFGHIJKLMNOPQRSTUVWXYZ_".toCharArray();
	private final static char[] DIGITS_DOT = "0123456789.".toCharArray();
	private final static char[] DIGITS = "0123456789".toCharArray();
	private final static char[] ALPHANUMERICS = "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ_".toCharArray();
	private final static char[] JUNK = { ' ', '\t', '\r' };
	
	/**
	 * Méthode publique permettant la tokenization
	 * @param chars stream de caractères à tokeniser
	 * @return une liste de Token
	 * @see StreamChars
	 * @see Token
	 */
	public static List<Token> tokenize(StreamChars chars) {
		try {
			return new Tokenizer(chars).start();
		} catch (SyntaxError e) {
			Shagolur.error("Error during tokenization :");
			SpellParser.printStackTraces(e);
			return Collections.emptyList();
		}
	}
	
	private final StreamChars chars;
	private int line;
	
	private Tokenizer(StreamChars chars) {
		this.chars = chars;
	}
	
	private List<Token> start() throws SyntaxError {
		List<Token> tokens = new LinkedList<>();
		while(chars.hasNext()) {
			char current = chars.next();
			if(contains(ALPHA, current)) {
				// identifier ou token
				String identifier = composeIdentifier(current);
				if(identifier.equals("true") || identifier.equals("TRUE")) {
					tokens.add(new Token(TokenType.VALUE_BOOLEAN, true));
					continue;
				}
				if(identifier.equals("false") || identifier.equals("FALSE")) {
					tokens.add(new Token(TokenType.VALUE_BOOLEAN, false));
					continue;
				}
				if(KEYWORD_TOKEN.containsKey(identifier)) {
					tokens.add(KEYWORD_TOKEN.get(identifier).convert());
				} else {
					tokens.add(TokenType.IDENTIFIER.convert(identifier));
				}
			}
			else if(current == '%') {
				String value = composeIdentifier(chars.next());
				tokens.add(TokenType.VARIABLE.convert(value));
			}
			else if(contains(DIGITS, current)) {
				// Nombre indéterminé
				tokens.add(composeNumber(current));
			}
			else if(SIMPLE_TOKENS.containsKey(current)) {
				// Nombre indéterminé
				tokens.add(SIMPLE_TOKENS.get(current).convert());
			}
			else if(current == '\n') {
				// new line
				line ++;
			}
			else if(current == '#' || current == '/') {
				comment(current);
			}
			else if(current == '\"') {
				// Début d'un string
				tokens.add(composeStringValueToken());
			}
			else if(!contains(JUNK, current)) {
				// tout le reste
				Shagolur.error("Unrecognized char : '"+current+"'.");
			}
		}
		tokens.add(TokenType.EOF.convert());
		return tokens;
	}
	
	/**
	 * Nombres commençant par un 0.
	 */
	private Token composeNumberZero() throws SyntaxError {
		if(chars.hasNext() && contains(ALPHANUMERICS, chars.peek())) {
			throw new SyntaxError(line, "Unexpected char '0' here.");
		}
		if(chars.hasNext() && chars.peek() == '.') {
			chars.junk();
			StringBuilder digits = new StringBuilder("0.");
			char current;
			while(chars.hasNext() && contains(DIGITS, current = chars.peek())) {
				digits.append(current);
				chars.junk();
			}
			return new Token(TokenType.VALUE_NUMERICAL, Double.parseDouble(digits.toString()));
		}
		return new Token(TokenType.VALUE_NUMERICAL, 0);
	}
	
	/**
	 * Chaînes de caractères
	 */
	private Token composeStringValueToken() {
		StringBuilder sb = new StringBuilder();
		boolean escapeNext = false;
		while(chars.hasNext()) {
			char current = chars.next();
			if(escapeNext) {
				escapeNext = false;
				sb.append(current);
			}
			else if(current == '\\') {
				escapeNext = true;
				sb.append(current);
			}
			else if(current == '\"') {
				break;
			}
			else {
				sb.append(current);
			}
		}
		return new Token(TokenType.VALUE_STRING, sb.toString());
	}
	
	/**
	 * Construction des identifiers
	 */
	private String composeIdentifier(char first) {
		char current;
		StringBuilder sb = new StringBuilder().append(first);
		while(chars.hasNext() && contains(ALPHANUMERICS, current = chars.peek())) {
			sb.append(current);
			chars.junk();
		}
		if(chars.peek() == '!') {
			chars.junk();
			sb.append("!");
		}
		return sb.toString();
	}
	
	/**
	 * Construction de tous les nombres
	 */
	private Token composeNumber(char first) throws SyntaxError {
		if(first == '0')
			return composeNumberZero();
		char current;
		StringBuilder number = new StringBuilder().append(first);
		boolean decimal = false;
		while(chars.hasNext() && contains(DIGITS_DOT, current = chars.peek())) {
			chars.junk();
			if(current == '.') {
				if(decimal)
					throw new SyntaxError(line, "Incorrect number format. Unexpected '.'.");
				decimal = true;
				number.append(".");
			} else {
				number.append(current);
			}
		}
		return new Token(TokenType.VALUE_NUMERICAL, Double.parseDouble(number.toString()));
	}
	
	private void comment(char current) throws SyntaxError {
		if(current == '#') {
			while(chars.hasNext() && chars.next() != '\n') {/* empty while block because we do everything in the condition*/}
			line++;
			return;
		} else if(current == '/' && chars.next() == '*') {
			char curr;
			while(chars.hasNext()) {
				curr = chars.next();
				if(curr == '\n') {
					line++;
				} else if(curr == '*') {
					if(chars.peek() == '/') {
						chars.junk();
						return;
					}
				}
			}
			return;
		}
		throw new SyntaxError(line, "Unexpected character : '"+current+"'.");
	}
	
	/**
	 * Check if a characters array contains a specific character
	 * @param array char array
	 * @param c character to test the existence of
	 * @return true if the array contains the specific character
	 */
	private static boolean contains(char[] array, char c) {
		char cx = Character.toUpperCase(c);
		for(char a : array)
			if(a == cx)
				return true;
		return false;
	}
	
}
