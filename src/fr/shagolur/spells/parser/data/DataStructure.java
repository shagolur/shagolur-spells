package fr.shagolur.spells.parser.data;

import fr.shagolur.spells.parser.elements.StreamTokens;
import fr.shagolur.spells.parser.elements.PreviousToken;
import fr.shagolur.spells.parser.elements.Token;
import fr.shagolur.spells.parser.elements.TokenType;
import fr.shagolur.spells.parser.errors.BadTokenError;
import fr.shagolur.spells.parser.errors.SyntaxError;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DataStructure {
	
	private final Map<String, Double> dataNumbers = new HashMap<>();
	private final Map<String, String> dataStrings = new HashMap<>();
	private final Map<String, Boolean> dataBools = new HashMap<>();
	private final Map<String, DataStructure> children = new HashMap<>();
	private final Map<String, List<DataStructure>> childrenBlocks = new HashMap<>();
	
	private boolean contains(String key) {
		return dataNumbers.containsKey(key)
				|| dataStrings.containsKey(key)
				|| dataBools.containsKey(key)
				|| children.containsKey(key)
				|| childrenBlocks.containsKey(key);
	}
	
	public void register(@NotNull String key, String stringValue) throws SyntaxError {
		if(contains(key))
			throw new SyntaxError("The key \""+key+"\" in datastructure as already been declared.");
		dataStrings.put(key, stringValue);
	}
	
	public void register(@NotNull String key, double doubleValue) throws SyntaxError {
		if(contains(key))
			throw new SyntaxError("The key \""+key+"\" in datastructure as already been declared.");
		dataNumbers.put(key, doubleValue);
	}
	
	public void register(@NotNull String key, boolean boolValue) throws SyntaxError {
		if(contains(key))
			throw new SyntaxError("The key \""+key+"\" in datastructure as already been declared.");
		dataBools.put(key, boolValue);
	}
	
	public void register(@NotNull String key, DataStructure child) throws SyntaxError {
		if(contains(key))
			throw new SyntaxError("The key \""+key+"\" in datastructure as already been declared.");
		children.put(key, child);
	}
	
	public void register(@NotNull String key, List<DataStructure> children) throws SyntaxError {
		if(contains(key))
			throw new SyntaxError("The key \""+key+"\" in datastructure as already been declared.");
		childrenBlocks.put(key, children);
	}
	
	public @Nullable String getString(@NotNull String key) {
		return dataStrings.get(key);
	}
	public String getString(@NotNull String key, @Nullable String defaultValue) {
		return dataStrings.getOrDefault(key, defaultValue);
	}
	
	public double getDouble(@NotNull String key, double defaultValue) {
		return dataNumbers.getOrDefault(key, defaultValue);
	}
	
	public boolean getBoolean(@NotNull String key, boolean defaultValue) {
		return dataBools.getOrDefault(key, defaultValue);
	}
	
	public @Nullable DataStructure getDataStructure(@NotNull String key) {
		return children.get(key);
	}
	
	public List<DataStructure> getDataStructureList(@NotNull String key) {
		return childrenBlocks.getOrDefault(key, Collections.emptyList());
	}
	
	@PreviousToken(allowed = TokenType.BLOCK_START)
	public static DataStructure generate(StreamTokens tokens) throws BadTokenError, SyntaxError {
		DataStructure ds = new DataStructure();
		while(tokens.isNotFuture(TokenType.BLOCK_END)) {
			String dataKey = tokens.readIdentifier();
			tokens.junkOrThrow(TokenType.COLON, "Expected a ':' after a datakey in a datablock.");
			Token token = tokens.next();
			switch(token.getType()) {
				case VALUE_STRING -> ds.register(dataKey, token.getValString());
				case VALUE_NUMERICAL -> ds.register(dataKey, token.getValDouble());
				case BLOCK_START -> ds.register(dataKey, DataStructure.generate(tokens));
				case IDENTIFIER -> {
					String val = token.getValString();
					if(val.equalsIgnoreCase("true"))
						ds.register(dataKey, true);
					else if(val.equalsIgnoreCase("false"))
						ds.register(dataKey, false);
				}
				case ARRAY_START -> {
					if(tokens.isFutureConsume(TokenType.ARRAY_END))
						break; // we allow "[]" as valid list.
					List<DataStructure> dsChildren = new ArrayList<>();
					// Get all the chidren
					do {
						tokens.junkOrThrow(TokenType.BLOCK_START, "datastructure of key \""+dataKey+"\" expected a '{' after a '['.");
						dsChildren.add(DataStructure.generate(tokens));
					} while(tokens.isFutureConsume(TokenType.COMMA));
					tokens.junkOrThrow(TokenType.ARRAY_END, "datastructure of key \""+dataKey+"\" expected a ']' after a '[' and it's content.");
					// Register the list
					ds.register(dataKey, dsChildren);
				}
				default -> throw new BadTokenError(token, "This token should have been a STRING or DOUBLE or BOOL in a datablock, for key \""+dataKey+"\"");
			}
			tokens.junkOrThrow(TokenType.SEMICOLON, "Expected a ';' after a data key and value.");
		}
		tokens.junkOrThrow(TokenType.BLOCK_END, "Expected a '}' after the end of the data block.");
		return ds;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		boolean first = true;
		for(String key : dataStrings.keySet()) {
			if(first) first = false; else sb.append("; ");
			sb.append(key).append("->\"").append(dataStrings.get(key)).append("\"");
		}
		for(String key : dataNumbers.keySet()) {
			if(first) first = false; else sb.append("; ");
			sb.append(key).append("->").append(dataNumbers.get(key));
		}
		for(String key : dataBools.keySet()) {
			if(first) first = false; else sb.append("; ");
			sb.append(key).append("->").append(dataBools.get(key));
		}
		for(String key : children.keySet()) {
			if(first) first = false; else sb.append("; ");
			sb.append(key).append("->").append(children.get(key));
		}
		for(String key : childrenBlocks.keySet()) {
			if(first) first = false; else sb.append("; ");
			sb.append(key).append("->").append(Arrays.toString(childrenBlocks.get(key).toArray()));
		}
		return "DATA[" + sb + "]";
	}
}
