package fr.shagolur.spells.parser.elements;

import org.bukkit.Location;
import org.bukkit.util.Vector;
import org.jetbrains.annotations.Nullable;

import java.util.function.BiFunction;

public enum Operator {
	
	PLUS(false,
			Double::sum,
			(d, l)->l.toVector().add(new Vector(d,d,d)).toLocation(l.getWorld()),
			(a,b)->a.toVector().add(b.toVector()).toLocation(a.getWorld()),
			(a, b)->a||b
	),
	
	MINUS(false,
			(a,b)->a-b,
			(d,l)->l.toVector().subtract(new Vector(d,d,d)).toLocation(l.getWorld()),
			(a,b)->a.toVector().subtract(b.toVector()).toLocation(a.getWorld()),
			(a,b)->a||!b
	),
	
	TIMES(true,
			(a,b)->a*b,
			(d,l)->l.toVector().multiply(d).toLocation(l.getWorld()),
			(a,b)->a.toVector().multiply(b.toVector()).toLocation(a.getWorld()),
			(a,b)->a&&b
	),
	
	DIV(true,
			(a,b)->a/b,
			(d,l)->l.toVector().divide(new Vector(d,d,d)).toLocation(l.getWorld()),
			(a, b)->a.toVector().divide(b.toVector()).toLocation(a.getWorld()),
			(a, b)->a&&!b
	);
	
	public final boolean prio;
	public final BiFunction<Double, Double, Double> fDDD;
	public final BiFunction<Double, Location, Location> fDLL;
	public final BiFunction<Location, Location, Location> fLLL;
	public final BiFunction<Boolean, Boolean, Boolean> fBBB;
	
	Operator(
			boolean prio,
			BiFunction<Double, Double, Double> fDDD,
			BiFunction<Double, Location, Location> fDLL,
			BiFunction<Location, Location, Location> fLLL,
			BiFunction<Boolean, Boolean, Boolean> fBBB
	) {
		this.prio = prio;
		this.fDDD = fDDD;
		this.fDLL = fDLL;
		this.fLLL = fLLL;
		this.fBBB = fBBB;
	}
	
	public static @Nullable Operator fromString(String str) {
		return switch (str) {
			case "+" -> PLUS;
			case "-" -> MINUS;
			case "*" -> TIMES;
			case "/" -> DIV;
			default -> null;
		};
	}
	
}
