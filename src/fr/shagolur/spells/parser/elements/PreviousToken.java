package fr.shagolur.spells.parser.elements;

import fr.shagolur.spells.parser.elements.TokenType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@Retention(RetentionPolicy.SOURCE)
@Target({ElementType.METHOD})
public @interface PreviousToken {
	
	TokenType[] allowed() default {};
	
}
