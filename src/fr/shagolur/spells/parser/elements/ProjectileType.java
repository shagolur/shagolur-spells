package fr.shagolur.spells.parser.elements;

public enum ProjectileType {
	
	FIREBALL,
	ARROW,
	ENTITY;
	
	public static ProjectileType fromString(String value) {
		return switch (value) {
			case "fireball" -> FIREBALL;
			case "arrow" -> ARROW;
			default -> null;
		};
	}
	
	public static String allowedValues() {
		return "{<none>, \"fireball\", \"arrow\"}";
	}
	
}
