package fr.shagolur.spells.parser.elements;

import fr.shagolur.core.Shagolur;
import fr.shagolur.core.entities.SEntity;
import fr.shagolur.core.entities.SEntityType;
import org.bukkit.entity.Entity;

public record Selector(SelectorType type, boolean excludeSelf) {
	
	public static Selector fromString(String str) {
		boolean excludeSelf = false;
		if(str.endsWith("!")) {
			excludeSelf = true;
			str = str.substring(0, str.length() - 1);
		}
		SelectorType type = SelectorType.fromString(str);
		if(type == null)
			return null;
		return new Selector(type, excludeSelf);
	}
	
	public boolean accepts(Entity source, Entity entity) {
		if(source == entity) {
			if(excludeSelf)
				return false;
			if(type == SelectorType.SELF)
				return true;
		}
		SEntity target = Shagolur.getEntitiesManager().getSEntity(entity);
		if(target == null)
			return false;
		
		return switch (type) {
			case SELF -> false;
			case MOBS -> target.getCategory() == SEntityType.MOB;
			case SUPER_MOBS -> target.getCategory() == SEntityType.SUPER_MOB;
			case BOSSES -> target.getCategory() == SEntityType.BOSS;
			case MONSTERS -> target.getCategory() == SEntityType.MOB
					|| target.getCategory() == SEntityType.SUPER_MOB
					|| target.getCategory() == SEntityType.BOSS;
			case PROJECTILES -> target.getCategory() == SEntityType.PROJECTILE;
			case SPELLS -> target.getCategory() == SEntityType.SPELL;
			case PLAYERS -> target.getCategory() == SEntityType.PLAYER;
			case ALL -> true;
		};
	}
	
}
