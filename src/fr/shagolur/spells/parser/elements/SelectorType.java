package fr.shagolur.spells.parser.elements;

import java.util.List;
import java.util.stream.Stream;

public enum SelectorType {
	
	SELF("self", "source"),
	//ALLIES("allies", "friends"),
	MOBS("mobs", "monsters"),
	SUPER_MOBS("supermobs", "super-mobs", "super_mobs"),
	BOSSES("boss", "bosses"),
	MONSTERS("monsters", "enemies"),
	//INVOCATIONS("invocs", "invocations"),
	PROJECTILES("projectiles"),
	SPELLS("spells"),
	PLAYERS("players"),
	ALL("all", "everything", "everyone");
	
	private final List<String> aliases;
	
	SelectorType(String... aliases) {
		this.aliases = List.of(aliases);
	}
	
	public static SelectorType fromString(String aliase) {
		String aliaseLowered = aliase.toLowerCase();
		return Stream.of(values())
				.filter(s -> s.aliases.contains(aliaseLowered))
				.findFirst()
				.orElse(null);
	}
	
}
