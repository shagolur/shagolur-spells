package fr.shagolur.spells.parser.elements;

import fr.shagolur.core.entities.SEntity;
import fr.shagolur.spells.parser.errors.SpellRuntimeError;
import fr.shagolur.spells.parser.expressions.Type;
import fr.shagolur.spells.spells.SpellCaster;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.jetbrains.annotations.NotNull;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Map;

public class SpellExecutionContext {
	
	private static final NumberFormat nf = new DecimalFormat("##.#");
	public static final String CASTER = "caster";
	
	private final SpellCaster caster;
	
	private final Map<String, Double> numbers = new HashMap<>();
	private final Map<String, Boolean> booleans = new HashMap<>();
	private final Map<String, SEntity> entities = new HashMap<>();
	private final Map<String, Location> locations = new HashMap<>();
	
	public SpellExecutionContext(SpellCaster caster) {
		this.caster = caster;
	}
	
	public SpellCaster getCaster() {
		return caster;
	}
	
	public void remove(String varName) {
		numbers.remove(varName);
		entities.remove(varName);
		locations.remove(varName);
		booleans.remove(varName);
	}
	
	public SpellExecutionContext inherit() {
		SpellExecutionContext child = new SpellExecutionContext(caster);
		child.numbers.putAll(numbers);
		child.entities.putAll(entities);
		child.locations.putAll(locations);
		child.booleans.putAll(booleans);
		return child;
	}
	
	// Numbers
	
	public void setNumber(String varName, double number) {
		remove(varName);
		numbers.put(varName, number);
	}
	public double getNumber(String varName) throws SpellRuntimeError {
		if(!numbers.containsKey(varName))
			throw new SpellRuntimeError("Unrecognized number variable %"+varName+".");
		return numbers.get(varName);
	}
	
	// Entities
	
	public void setEntity(String varName, @NotNull SEntity entity) {
		remove(varName);
		entities.put(varName, entity);
	}
	public @NotNull SEntity getEntity(String varName) throws SpellRuntimeError {
		if(varName.equals(CASTER))
			return caster.getSEntity();
		if(!entities.containsKey(varName))
			throw new SpellRuntimeError("Unrecognized entity variable %"+varName+".");
		return entities.get(varName);
	}
	
	// Locations
	
	public void setPosition(String varName, Entity en) {
		setPosition(varName, en.getLocation());
	}
	public void setPosition(String varName, Location loc) {
		remove(varName);
		locations.put(varName, loc);
	}
	public @NotNull Location getPosition(String varName) throws SpellRuntimeError {
		if(varName.equals(CASTER))
			return caster.getSEntity().getLocation();
		if(entities.containsKey(varName))
			return entities.get(varName).getLocation();
		if(!locations.containsKey(varName))
			throw new SpellRuntimeError("Unrecognized LOCATION variable %"+varName+".");
		return locations.get(varName);
	}
	
	// Booleans
	
	public void setBoolean(String varName, boolean val) {
		remove(varName);
		booleans.put(varName, val);
	}
	public boolean getBoolean(String varName) throws SpellRuntimeError {
		if(!booleans.containsKey(varName))
			throw new SpellRuntimeError("Unrecognized boolean variable %"+varName+".");
		return booleans.get(varName);
	}
	
	// Any
	
	public String getAsString(String varName) {
		if(varName.equals(CASTER))
			return caster.getSEntity().getName();
		if(entities.containsKey(varName))
			return entities.get(varName).getName();
		if(numbers.containsKey(varName))
			return nf.format(numbers.get(varName));
		if(booleans.containsKey(varName))
			return nf.format(booleans.get(varName) ? "TRUE" : "FALSE");
		if(locations.containsKey(varName)) {
			Location loc = locations.get(varName);
			return "("+nf.format(loc.getX())+", "+nf.format(loc.getY()) + ", " + nf.format(loc.getZ()) + ")";
		}
		return "null";
	}
	
	public Object getAsAny(String varName) throws SpellRuntimeError {
		if(varName.equals(CASTER))
			return caster.getSEntity();
		if(entities.containsKey(varName))
			return entities.get(varName);
		if(numbers.containsKey(varName))
			return numbers.get(varName);
		if(booleans.containsKey(varName))
			return booleans.get(varName);
		if(locations.containsKey(varName))
			return locations.get(varName);
		throw new SpellRuntimeError("Could not find object of variable name '%"+varName+"'.");
	}
	
	public Type getTypeOf(String varName) throws SpellRuntimeError {
		if(varName.equals(CASTER) || entities.containsKey(varName))
			return Type.ENTITY;
		if(numbers.containsKey(varName))
			return Type.DOUBLE;
		if(booleans.containsKey(varName))
			return Type.BOOLEAN;
		if(locations.containsKey(varName))
			return Type.LOCATION;
		throw new SpellRuntimeError("Could not find type of variable name '%"+varName+"'.");
	}
	
	public boolean hasVariable(String varName) {
		if(varName.equals(CASTER))
			return true;
		return entities.containsKey(varName)
				|| numbers.containsKey(varName)
				|| booleans.containsKey(varName)
				|| locations.containsKey(varName);
	}
	
}
