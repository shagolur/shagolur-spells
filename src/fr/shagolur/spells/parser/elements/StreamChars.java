package fr.shagolur.spells.parser.elements;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class StreamChars {
	
	private final char[] chars;
	private int index = 0;
	
	public StreamChars(String string) {
		this.chars = string.toCharArray();
	}
	
	public StreamChars(File file) {
		try(BufferedReader r = new BufferedReader(new FileReader(file))) {
			StringBuilder b = new StringBuilder();
			String line;
			while((line = r.readLine()) != null) {
				b.append(line).append('\n');
			}
			this.chars = b.toString().toCharArray();
		} catch (IOException e) {
			throw new IllegalArgumentException("Could not open file " + e.getMessage());
		}
	}
	
	public char peek() {
		return chars[index];
	}
	
	public boolean hasNext() {
		return index < chars.length;
	}
	
	public char next() {
		return chars[index++];
	}
	
	public void junk() {
		index++;
	}
	
	
}
