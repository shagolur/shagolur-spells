package fr.shagolur.spells.parser.elements;

import fr.shagolur.spells.parser.errors.BadTokenError;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class StreamTokens {
	
	private final List<Token> tokens;
	private int index = 0;
	
	public StreamTokens(List<Token> tokens) {
		this.tokens = Collections.unmodifiableList(tokens);
	}
	
	public void reverseAndSelf() {
		index--;
		if(index < 0)
			throw new RuntimeException("reverse() call wen into negative !");
	}
	
	public Set<TokenType> getAllTypes() {
		return tokens.stream().map(Token::getType).collect(Collectors.toSet());
	}
	
	public Token peek() {
		return tokens.get(index);
	}
	
	public boolean hasNext() {
		return index < tokens.size() && peek().getType() != TokenType.EOF;
	}
	
	public Token next() {
		return tokens.get(index++);
	}
	
	public void junk() {
		index++;
	}
	
	public boolean isFuture(TokenType type) {
		return hasNext() && peek().getType() == type;
	}
	public boolean isNotFuture(TokenType type) {
		return hasNext() && peek().getType() != type;
	}
	
	public void junkOrThrow(TokenType type, String errorMessage) throws BadTokenError {
		if(peek().getType() != type)
			throw new BadTokenError(peek(), errorMessage);
		junk();
	}
	
	public boolean doesFollow(TokenType... types) {
		int index = this.index;
		if(index + types.length >= this.tokens.size())
			return false;
		for(int i = index; i < index + types.length; i++) {
			if(tokens.get(i).getType() != types[i - index])
				return false;
		}
		return true;
	}
	
	public boolean isFutureConsume(TokenType type) {
		if(isFuture(type)) {
			junk();
			return true;
		}
		return false;
	}
	
	public boolean canFindTokenBeforeAnother(TokenType first, TokenType... seconds) {
		List<TokenType> others = Arrays.asList(seconds);
		int indexCp = index;
		while(indexCp < tokens.size()) {
			TokenType current = tokens.get(indexCp).getType();
			if(current == first)
				return true;
			if(others.contains(current))
				return false;
			indexCp++;
		}
		return false;
	}
	
	public String readVariableName() throws BadTokenError {
		Token t = next();
		if(t.getType() != TokenType.VARIABLE)
			throw new BadTokenError(t, "expected variable.");
		return t.getValString();
	}
	public String readStringValue() throws BadTokenError {
		Token t = next();
		if(t.getType() != TokenType.VALUE_STRING)
			throw new BadTokenError(t, "expected strin value.");
		return t.getValString();
	}
	public String readIdentifier() throws BadTokenError {
		Token t = next();
		if(t.getType() != TokenType.IDENTIFIER)
			throw new BadTokenError(t, "expected identifier.");
		return t.getValString();
	}
	public double readNumericValue() throws BadTokenError {
		Token t = next();
		if(t.getType() != TokenType.VALUE_NUMERICAL)
			throw new BadTokenError(t, "expected numerical value.");
		return t.getValDouble();
	}
	public Operator readOperator() throws BadTokenError {
		Token t = next();
		return switch(t.getType()) {
			case OPERATOR_ADD -> Operator.PLUS;
			case OPERATOR_SUB -> Operator.MINUS;
			case OPERATOR_MUL -> Operator.TIMES;
			case OPERATOR_DIV -> Operator.DIV;
			default -> throw new BadTokenError(t, "Expected an operator.");
		};
	}
	
}
