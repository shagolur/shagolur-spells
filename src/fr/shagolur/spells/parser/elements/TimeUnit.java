package fr.shagolur.spells.parser.elements;

import javax.annotation.Nullable;
import java.util.Set;
import java.util.stream.Stream;

public enum TimeUnit {
	
	MS(0.001, "ms", "milliseconds", "millisecond"),
	SECS(1, "s", "sec", "secs", "second", "seconds"),
	MINS(60, "m", "min", "mins", "minute", "minutes"),
	HOURS(3600, "h", "hr", "hrs", "hours", "hour");
	
	private final Set<String> aliases;
	private final double seconds;
	
	TimeUnit(double seconds, String... aliases) {
		this.seconds = seconds;
		this.aliases = Set.of(aliases);
	}
	
	public static @Nullable TimeUnit fromString(String str) {
		return Stream.of(values()).filter(u -> u.aliases.contains(str)).findFirst().orElse(null);
	}
	
	public double toSeconds(double amount) {
		return amount * seconds;
	}
	public long toSecondsTicks(double amount) {
		return (long)(20 * toSeconds(amount));
	}
	
}
