package fr.shagolur.spells.parser.elements;

public class Token {
	
	private final TokenType type;
	
	private String valString;
	private double valDouble;
	private boolean valBool;
	
	public Token(TokenType type) {
		this.type = type;
	}
	public Token(TokenType type, String val) {
		this.type = type;
		this.valString = val;
	}
	
	public Token(TokenType type, double val) {
		this.type = type;
		this.valDouble = val;
	}
	
	public Token(TokenType type, boolean val) {
		this.type = type;
		this.valBool = val;
	}
	
	public Token(Token clone) {
		this.type = clone.type;
		this.valDouble = clone.valDouble;
		this.valString = clone.valString;
		this.valBool = clone.valBool;
	}
	
	/**
	 * Get the type of the token
	 * @return the TokenType corresponding to the token
	 */
	public TokenType getType() {
		return type;
	}
	
	public String getValString() {
		return valString;
	}
	
	public double getValDouble() {
		return valDouble;
	}
	
	public boolean getValBoolean() {
		return valBool;
	}
	
	public String toString() {
		if(type == TokenType.VARIABLE)
			return "%"+valString;
		if(type == TokenType.IDENTIFIER)
			return valString;
		if(type == TokenType.VALUE_STRING)
			return "STR("+valString+")";
		if(type == TokenType.VALUE_NUMERICAL)
			return "NUM_("+valDouble+")";
		if(type == TokenType.VALUE_BOOLEAN)
			return "BOOL_("+valBool+")";
		return type.name();
	}
	
	
}
