package fr.shagolur.spells.parser.elements;

public enum TokenType {

	AFTER,
	AROUND,
	AS,
	AT,
	DO,
	DURING,
	FROM,
	IN,
	OF,
	TO,
	SET,
	WITH,
	
	TELEPORT,
	APPLY,
	SEND,
	THROW,
	SPAWN,
	DAMAGE,
	HEAL,
	STOP,
	RETURN,
	BREAK,
	EXECUTE,
	REMOVE,
	
	METH_GET_LEVEL,
	METH_LOCATION,
	METH_DISTANCE,
	METH_EXISTS,
	METH_RAYCAST,
	
	IF,
	ELSE,
	FOR,
	LOOP,
	
	OPERATOR_ADD, // +
	OPERATOR_SUB, // -
	OPERATOR_MUL, // *
	OPERATOR_DIV, // /
	
	PAREN_OPEN, // (
	PAREN_CLOSE,// )
	BLOCK_START,// {
	BLOCK_END,  // }
	ARRAY_START,// [
	ARRAY_END,  // ]
	COLON,      // :
	SEMICOLON,  // ;
	EQUAL,      // =
	COMMA,      // ,
	
	VALUE_BOOLEAN,
	VALUE_STRING,
	VALUE_NUMERICAL,
	IDENTIFIER,
	VARIABLE,
	
	EOF;
	
	public Token convert() {
		return new Token(this);
	}
	public Token convert(String val) {
		return new Token(this, val);
	}
	public Token convert(double val) {
		return new Token(this, val);
	}
	
	public boolean isOperator() {
		return this == OPERATOR_ADD || this == OPERATOR_SUB || this == OPERATOR_MUL || this == OPERATOR_DIV;
	}

}
