package fr.shagolur.spells.parser.errors;

import fr.shagolur.spells.parser.elements.Token;

public class BadTokenError extends Exception {
	public BadTokenError(Token token, String message) {
		super("Unexpected token " + token + " : " + message);
	}
}
