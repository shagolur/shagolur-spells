package fr.shagolur.spells.parser.errors;

public class SpellRuntimeError extends Exception {
	public SpellRuntimeError(String message) {
		super(message);
	}
}
