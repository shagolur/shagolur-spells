package fr.shagolur.spells.parser.errors;

public class SpellTerminated extends SpellRuntimeError {
	public SpellTerminated() {
		super("no error occured.");
	}
}
