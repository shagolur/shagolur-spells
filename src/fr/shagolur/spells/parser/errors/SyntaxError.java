package fr.shagolur.spells.parser.errors;

public class SyntaxError extends Exception {
	public SyntaxError(int line, String error) {
		super("SyntaxError line " + line + " : " + error);
	}
	
	public SyntaxError(String error) {
		super(error);
	}
	
}
