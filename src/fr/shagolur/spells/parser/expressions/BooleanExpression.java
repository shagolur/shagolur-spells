package fr.shagolur.spells.parser.expressions;

import fr.shagolur.spells.parser.elements.SpellExecutionContext;
import fr.shagolur.spells.parser.errors.SpellRuntimeError;

public class BooleanExpression extends Expression {
	
	private final boolean value;
	
	public BooleanExpression(boolean value) {
		this.value = value;
	}
	
	@Override
	public Boolean get(SpellExecutionContext context) throws SpellRuntimeError {
		return value;
	}
	
	@Override
	public Type getType(SpellExecutionContext context) throws SpellRuntimeError {
		return Type.BOOLEAN;
	}
	
	@Override
	public String toString() {
		return "b{" + value + "}";
	}
}
