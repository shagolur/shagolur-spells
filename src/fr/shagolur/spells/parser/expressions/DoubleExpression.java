package fr.shagolur.spells.parser.expressions;

import fr.shagolur.spells.parser.elements.SpellExecutionContext;
import fr.shagolur.spells.parser.errors.SpellRuntimeError;

public class DoubleExpression extends Expression {
	
	private final double value;
	
	public DoubleExpression(double value) {
		this.value = value;
	}
	
	@Override
	public Double get(SpellExecutionContext context) throws SpellRuntimeError {
		return value;
	}
	
	@Override
	public Type getType(SpellExecutionContext context) throws SpellRuntimeError {
		return Type.DOUBLE;
	}
	
	@Override
	public String toString() {
		return "d{" + value + "}";
	}
	
}
