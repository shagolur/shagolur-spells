package fr.shagolur.spells.parser.expressions;

import fr.shagolur.spells.parser.elements.Operator;
import fr.shagolur.spells.parser.elements.SpellExecutionContext;
import fr.shagolur.spells.parser.elements.StreamTokens;
import fr.shagolur.spells.parser.elements.Token;
import fr.shagolur.spells.parser.elements.TokenType;
import fr.shagolur.spells.parser.errors.BadTokenError;
import fr.shagolur.spells.parser.errors.SpellRuntimeError;
import fr.shagolur.spells.parser.errors.SyntaxError;
import fr.shagolur.spells.parser.expressions.methods.DistanceMethod;
import fr.shagolur.spells.parser.expressions.methods.ExistsMethod;
import fr.shagolur.spells.parser.expressions.methods.GetLevelMethod;
import fr.shagolur.spells.parser.expressions.methods.LocationMethod;
import fr.shagolur.spells.parser.expressions.methods.RaytraceMethod;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class Expression {
	
	public abstract Object get(SpellExecutionContext context) throws SpellRuntimeError;
	
	public abstract Type getType(SpellExecutionContext context) throws SpellRuntimeError;
	
	/*------------- CLASSES METHODS ---------------*/
	
	private static Expression parseSimpleMathExpression(List<Expression> elements, List<Operator> operators) {
		if(elements.size() == 1) {
			return elements.get(0);
		}
		Expression left = elements.get(0);
		elements.remove(0);
		Operator ope = operators.get(0);
		operators.remove(0);
		Expression right = parseSimpleMathExpression(elements, operators);
		return new OperativeExpression(left, ope, right);
	}
	
	private static Expression parseMathExpression(List<Expression> elements, List<Operator> operators) {
		assert elements.size() == operators.size() + 1 : "Expected elements.size to be 1 more than operators. " +
				"Got ("+elements.size()+")"+ Arrays.toString(elements.toArray()) + " / " +
				"(" + operators.size()+")"+ Arrays.toString(operators.toArray())+".";
		
		// Test if simple expression
		boolean hasPrio = false;
		for(Operator ope : operators) {
			if(ope.prio) {
				hasPrio = true;
				break;
			}
		}
		if(!hasPrio) {
			return parseSimpleMathExpression(elements, operators);
		}
		
		// Prio blocks
		List<Expression> leftElements = new ArrayList<>();
		List<Operator> leftOperators = new ArrayList<>();
		List<Expression> rightElements = new ArrayList<>();
		List<Operator> rightOperators = new ArrayList<>();
		Operator mainOperator = null;
		for(int n = 0; n < operators.size(); n++) {
			leftElements.add(elements.get(n));
			
			boolean prio = operators.get(n).prio;
			if(prio) {
				mainOperator = operators.get(n);
				
				//de n à max-1, tout va à droite.
				for(int i = n+1; i < operators.size(); i++) {
					rightElements.add(elements.get(i));
					rightOperators.add(operators.get(i));
				}
				rightElements.add(elements.get(operators.size()));
				break;
			} else {
				leftOperators.add(operators.get(n));
			}
		}
		
		assert mainOperator != null : "Could not define mainOperator in expression.";
		
		Expression left = parseMathExpression(leftElements, leftOperators);
		Expression right = parseMathExpression(rightElements, rightOperators);
		
		return new OperativeExpression(left, mainOperator, right);
	}
	
	private static Expression composeMathsExpression(List<Expression> elements, List<Operator> operators, StreamTokens tokens) throws SyntaxError, BadTokenError {
		Expression next = generateExpression(tokens);
		elements.add(next);
		if(tokens.peek().getType().isOperator()) {
			Operator operator = tokens.readOperator();
			operators.add(operator);
			return composeMathsExpression(elements, operators, tokens);
		}
		return parseMathExpression(elements, operators);
	}
	
	private static Expression tryToStartMathComposition(Expression first, StreamTokens tokens) throws SyntaxError, BadTokenError {
		if(tokens.peek().getType().isOperator()) {
			Operator operator = tokens.readOperator();
			List<Expression> expressions = new ArrayList<>(List.of(first));
			List<Operator> operators = new ArrayList<>(List.of(operator));
			return composeMathsExpression(expressions, operators, tokens);
		}
		return first;
	}
	
	public static Expression generateExpression(StreamTokens tokens) throws SyntaxError, BadTokenError {
		Token token = tokens.next();
		switch (token.getType()) {
			case PAREN_OPEN -> {
				Expression block = Expression.generateExpression(tokens);
				tokens.junkOrThrow(TokenType.PAREN_CLOSE, "Expected a ')' to close the parenthesis block.");
				return tryToStartMathComposition(block, tokens);
			}
			case VALUE_NUMERICAL -> {
				DoubleExpression expression = new DoubleExpression(token.getValDouble());
				return tryToStartMathComposition(expression, tokens);
			}
			case VALUE_BOOLEAN -> {
				boolean value = token.getValBoolean();
				//TODO logical operators
				return new BooleanExpression(value);
			}
			case VARIABLE -> {
				VariableExpression var = new VariableExpression(token.getValString());
				return tryToStartMathComposition(var, tokens);
			}
			case METH_LOCATION -> {
				return tryToStartMathComposition(LocationMethod.generate(tokens), tokens);
			}
			case METH_DISTANCE -> {
				return tryToStartMathComposition(DistanceMethod.generate(tokens), tokens);
			}
			case METH_EXISTS -> {
				return tryToStartMathComposition(ExistsMethod.generate(tokens), tokens);
			}
			case METH_RAYCAST -> {
				return tryToStartMathComposition(RaytraceMethod.generate(tokens), tokens);
			}
			case METH_GET_LEVEL -> {
				return tryToStartMathComposition(GetLevelMethod.generate(tokens), tokens);
			}
			default -> throw new BadTokenError(token, "Expected a token to start an expression.");
		}
	}
	
}
