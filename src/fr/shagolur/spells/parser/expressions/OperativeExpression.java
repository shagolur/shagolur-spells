package fr.shagolur.spells.parser.expressions;

import fr.shagolur.core.Shagolur;
import fr.shagolur.spells.parser.elements.Operator;
import fr.shagolur.spells.parser.elements.SpellExecutionContext;
import fr.shagolur.spells.parser.errors.SpellRuntimeError;
import org.bukkit.Location;

public class OperativeExpression extends Expression {
	
	private final Expression left, right;
	private final Operator operator;
	
	public OperativeExpression(Expression left, Operator operator, Expression right) {
		this.left = left;
		this.right = right;
		this.operator = operator;
	}
	
	@Override
	public Object get(SpellExecutionContext c) throws SpellRuntimeError {
		if(left.getType(c) == Type.DOUBLE) {
			if(right.getType(c) == Type.DOUBLE) {
				return operator.fDDD.apply((double)left.get(c), (double)right.get(c));
			}
			if(right.getType(c) == Type.LOCATION) {
				return operator.fDLL.apply((double)left.get(c), (Location) right.get(c));
			}
		}
		if(left.getType(c) == Type.LOCATION) {
			if(right.getType(c) == Type.DOUBLE) {
				return operator.fDLL.apply((double)right.get(c), (Location) left.get(c));
			}
			if(right.getType(c) == Type.LOCATION) {
				return operator.fLLL.apply((Location)left.get(c), (Location) right.get(c));
			}
		}
		if(left.getType(c) == Type.BOOLEAN) {
			if(right.getType(c) == Type.BOOLEAN) {
				return operator.fBBB.apply((boolean)right.get(c), (boolean) left.get(c));
			}
		}
		
		throw new SpellRuntimeError("Could not assert calculus for "+this+".");
	}
	
	@Override
	public Type getType(SpellExecutionContext c) throws SpellRuntimeError {
		if(left.getType(c) == Type.DOUBLE) {
			if(right.getType(c) == Type.DOUBLE)
				return Type.DOUBLE;
			if(right.getType(c) == Type.LOCATION)
				return Type.LOCATION;
		}
		if(left.getType(c) == Type.LOCATION) {
			if(right.getType(c) == Type.DOUBLE)
				return Type.LOCATION;
			if(right.getType(c) == Type.LOCATION)
				return Type.LOCATION;
		}
		if(left.getType(c) == Type.BOOLEAN)
			return Type.BOOLEAN;
		
		throw new SpellRuntimeError("Could not assert calculus type for "+left+" " + operator + " " + right+".");
	}
	
	@Override
	public String toString() {
		return "{" + left + " " + operator + " " + right + "}";
	}
}
