package fr.shagolur.spells.parser.expressions;

import fr.shagolur.spells.parser.errors.SpellRuntimeError;

public enum Type {
	
	LOCATION,
	BOOLEAN,
	ENTITY,
	DOUBLE;
	
	public Type mix(Type type) throws SpellRuntimeError {
		if(type == this)
			return this;
		if(this == LOCATION && type == DOUBLE || this == DOUBLE && type == LOCATION)
			return LOCATION;
		throw new SpellRuntimeError("Cannot mix types ["+this+"] and ["+type+"].");
	}
	
}
