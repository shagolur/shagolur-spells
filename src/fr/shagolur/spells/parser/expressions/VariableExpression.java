package fr.shagolur.spells.parser.expressions;

import fr.shagolur.spells.parser.elements.SpellExecutionContext;
import fr.shagolur.spells.parser.errors.SpellRuntimeError;

public class VariableExpression extends Expression {
	
	private final String varName;
	
	public VariableExpression(String varName) {
		this.varName = varName;
	}
	
	@Override
	public Object get(SpellExecutionContext context) throws SpellRuntimeError {
		return context.getAsAny(varName);
	}
	
	@Override
	public Type getType(SpellExecutionContext context) throws SpellRuntimeError {
		return context.getTypeOf(varName);
	}
	
	@Override
	public String toString() {
		return "%{" + varName + "}";
	}
	
}
