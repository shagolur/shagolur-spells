package fr.shagolur.spells.parser.expressions.methods;

import fr.shagolur.spells.parser.elements.PreviousToken;
import fr.shagolur.spells.parser.elements.SpellExecutionContext;
import fr.shagolur.spells.parser.elements.StreamTokens;
import fr.shagolur.spells.parser.elements.TokenType;
import fr.shagolur.spells.parser.errors.BadTokenError;
import fr.shagolur.spells.parser.errors.SpellRuntimeError;
import fr.shagolur.spells.parser.errors.SyntaxError;
import fr.shagolur.spells.parser.expressions.Expression;
import fr.shagolur.spells.parser.expressions.Type;
import org.bukkit.Location;

public class DistanceMethod extends Expression {
	
	private final Expression locA, locB;
	
	DistanceMethod(Expression locA, Expression locB) {
		this.locA = locA;
		this.locB = locB;
	}
	
	@Override
	public Double get(SpellExecutionContext context) throws SpellRuntimeError {
		if(locA.getType(context) != Type.LOCATION)
			throw new SpellRuntimeError("Bad type for expression " + locA + ". Expected LOCATION type for method DISTANCE(0).");
		if(locB.getType(context) != Type.LOCATION)
			throw new SpellRuntimeError("Bad type for expression " + locB + ". Expected LOCATION type for method DISTANCE(1).");
		Location a = (Location) locA.get(context);
		Location b = (Location) locB.get(context);
		return a.distance(b);
	}
	
	@Override
	public Type getType(SpellExecutionContext context) throws SpellRuntimeError {
		return Type.DOUBLE;
	}
	
	@PreviousToken(allowed = TokenType.METH_DISTANCE)
	public static DistanceMethod generate(StreamTokens tokens) throws SyntaxError, BadTokenError {
		tokens.junkOrThrow(TokenType.PAREN_OPEN, "Function DISTANCE needs a '(' after declaration.");
		Expression posA = Expression.generateExpression(tokens);
		tokens.junkOrThrow(TokenType.COMMA, "Expected a ',' after first argument of DISTANCE method.");
		Expression posB = Expression.generateExpression(tokens);
		tokens.junkOrThrow(TokenType.PAREN_CLOSE, "Expected a ')' after second argument of DISTANCE method.");
		return new DistanceMethod(posA, posB);
	}
	
	@Override
	public String toString() {
		return "DISTANCE(" + locA + " <-> " + locB + ")";
	}
}
