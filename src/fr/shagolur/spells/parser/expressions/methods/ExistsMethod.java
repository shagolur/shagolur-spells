package fr.shagolur.spells.parser.expressions.methods;

import fr.shagolur.spells.parser.elements.PreviousToken;
import fr.shagolur.spells.parser.elements.SpellExecutionContext;
import fr.shagolur.spells.parser.elements.StreamTokens;
import fr.shagolur.spells.parser.elements.TokenType;
import fr.shagolur.spells.parser.errors.BadTokenError;
import fr.shagolur.spells.parser.errors.SpellRuntimeError;
import fr.shagolur.spells.parser.errors.SyntaxError;
import fr.shagolur.spells.parser.expressions.Expression;
import fr.shagolur.spells.parser.expressions.Type;
import org.bukkit.Location;

public class ExistsMethod extends Expression {
	
	private final String varName;
	
	ExistsMethod(String varName) {
		this.varName = varName;
	}
	
	@Override
	public Boolean get(SpellExecutionContext context) throws SpellRuntimeError {
		return context.hasVariable(varName);
	}
	
	@Override
	public Type getType(SpellExecutionContext context) throws SpellRuntimeError {
		return Type.BOOLEAN;
	}
	
	@PreviousToken(allowed = TokenType.METH_DISTANCE)
	public static ExistsMethod generate(StreamTokens tokens) throws SyntaxError, BadTokenError {
		tokens.junkOrThrow(TokenType.PAREN_OPEN, "Function EXISTS needs a '(' after declaration.");
		String varName = tokens.readVariableName();
		tokens.junkOrThrow(TokenType.PAREN_CLOSE, "Expected a ')' after argument of EXISTS method.");
		return new ExistsMethod(varName);
	}
	
	@Override
	public String toString() {
		return "EXISTS?(" +varName + ")";
	}
}
