package fr.shagolur.spells.parser.expressions.methods;

import fr.shagolur.core.entities.SEntity;
import fr.shagolur.core.players.SPlayer;
import fr.shagolur.spells.parser.elements.PreviousToken;
import fr.shagolur.spells.parser.elements.SpellExecutionContext;
import fr.shagolur.spells.parser.elements.StreamTokens;
import fr.shagolur.spells.parser.elements.TokenType;
import fr.shagolur.spells.parser.errors.BadTokenError;
import fr.shagolur.spells.parser.errors.SpellRuntimeError;
import fr.shagolur.spells.parser.expressions.Expression;
import fr.shagolur.spells.parser.expressions.Type;

public class GetLevelMethod extends Expression {
	
	private final String entitySource;
	
	GetLevelMethod(String entitySource) {
		this.entitySource = entitySource;
	}
	
	@Override
	public Integer get(SpellExecutionContext context) throws SpellRuntimeError {
		SEntity entity = context.getEntity(entitySource);
		if(entity instanceof SPlayer)
			return ((SPlayer) entity).getLevel();
		return 1;
	}
	
	@Override
	public Type getType(SpellExecutionContext context) throws SpellRuntimeError {
		return Type.DOUBLE;
	}
	
	@PreviousToken(allowed = TokenType.METH_GET_LEVEL)
	public static GetLevelMethod generate(StreamTokens tokens) throws BadTokenError {
		tokens.junkOrThrow(TokenType.PAREN_OPEN, "GET_LEVEL method needs a '(' after declaration.");
		String source = tokens.readVariableName();
		tokens.junkOrThrow(TokenType.PAREN_CLOSE, "GET_LEVEL method needs a ')' after the source value.");
		return new GetLevelMethod(source);
	}
	
	@Override
	public String toString() {
		return "LEVEL_OF(%"+entitySource+")";
	}
}
