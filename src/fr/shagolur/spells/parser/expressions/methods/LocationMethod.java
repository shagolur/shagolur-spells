package fr.shagolur.spells.parser.expressions.methods;

import fr.shagolur.spells.parser.elements.PreviousToken;
import fr.shagolur.spells.parser.elements.SpellExecutionContext;
import fr.shagolur.spells.parser.elements.StreamTokens;
import fr.shagolur.spells.parser.elements.TokenType;
import fr.shagolur.spells.parser.errors.BadTokenError;
import fr.shagolur.spells.parser.errors.SpellRuntimeError;
import fr.shagolur.spells.parser.expressions.Expression;
import fr.shagolur.spells.parser.expressions.Type;
import org.bukkit.Location;

public class LocationMethod extends Expression {
	
	private String entitySource;
	private double x, y, z;
	private final boolean fromEntity;
	
	LocationMethod(String entitySource) {
		this.entitySource = entitySource;
		fromEntity = true;
	}
	
	LocationMethod(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
		fromEntity = false;
	}
	
	@Override
	public Location get(SpellExecutionContext context) throws SpellRuntimeError {
		// If we produces from an entity, we use the context
		if(fromEntity)
			return context.getPosition(entitySource);
		// If not, we just create a Bukkit Location
		return new Location(
				context.getCaster().getSEntity().getLocation().getWorld(),
				x, y, z
		);
	}
	
	@Override
	public Type getType(SpellExecutionContext context) throws SpellRuntimeError {
		return Type.LOCATION;
	}
	
	@PreviousToken(allowed = TokenType.METH_LOCATION)
	public static LocationMethod generate(StreamTokens tokens) throws BadTokenError {
		tokens.junkOrThrow(TokenType.PAREN_OPEN, "LOCATION method needs a '(' after declaration.");
		if(tokens.isFuture(TokenType.VARIABLE)) {
			String varName = tokens.readVariableName();
			tokens.junkOrThrow(TokenType.PAREN_CLOSE, "LOCATION method needs a ')' after variable.");
			return new LocationMethod(varName);
		} else if(tokens.isFuture(TokenType.VALUE_NUMERICAL)) {
			double x = tokens.readNumericValue();
			tokens.junkOrThrow(TokenType.COMMA, "LOCATION method needs a ',' after the x value.");
			double y = tokens.readNumericValue();
			tokens.junkOrThrow(TokenType.COMMA, "LOCATION method needs a ',' after the y value.");
			double z = tokens.readNumericValue();
			tokens.junkOrThrow(TokenType.PAREN_CLOSE, "LOCATION method needs a ')' after the z value.");
			return new LocationMethod(x, y, z);
		}
		throw new BadTokenError(tokens.peek(), "Expected a variable or a numerical value in a LOCATION method.");
	}
	
	@Override
	public String toString() {
		if(entitySource == null)
			return "LOCATION("+x+","+y+","+z+")";
		return "LOCATION(%"+entitySource+")";
	}
}
