package fr.shagolur.spells.parser.expressions.methods;

import fr.shagolur.core.entities.SEntity;
import fr.shagolur.spells.parser.elements.PreviousToken;
import fr.shagolur.spells.parser.elements.SpellExecutionContext;
import fr.shagolur.spells.parser.elements.StreamTokens;
import fr.shagolur.spells.parser.elements.TokenType;
import fr.shagolur.spells.parser.errors.BadTokenError;
import fr.shagolur.spells.parser.errors.SpellRuntimeError;
import fr.shagolur.spells.parser.errors.SyntaxError;
import fr.shagolur.spells.parser.expressions.DoubleExpression;
import fr.shagolur.spells.parser.expressions.Expression;
import fr.shagolur.spells.parser.expressions.Type;
import org.bukkit.FluidCollisionMode;
import org.bukkit.entity.LivingEntity;
import org.bukkit.util.RayTraceResult;

public class RaytraceMethod extends Expression {
	
	public static final double DEFAULT_DISTANCE = 20;
	
	private final Expression entity;
	private final String outVarPos;
	private final Expression distance;
	
	// SET <HIT?> = RAYTRACE(%ENTITE, %_OUT_position [, <distance>])
	RaytraceMethod(Expression entity, String outVarPos, Expression distance) {
		this.entity = entity;
		this.outVarPos = outVarPos;
		this.distance = distance;
	}
	
	@Override
	public Boolean get(SpellExecutionContext context) throws SpellRuntimeError {
		if(entity.getType(context) != Type.ENTITY)
			throw new SpellRuntimeError("Bad type for expression " + entity + ". Expected ENTITY type for method RAYTRACE(0).");
		if(distance.getType(context) != Type.DOUBLE)
			throw new SpellRuntimeError("Bad type for expression " + distance + ". Expected DOUBLE type for method RAYTRACE(2).");
		SEntity entity = (SEntity) this.entity.get(context);
		LivingEntity bukkitEntity = entity.getEntity();
		
		// Do the raycast
		RayTraceResult result = bukkitEntity.getWorld().rayTraceBlocks(
				bukkitEntity.getEyeLocation(),
				bukkitEntity.getEyeLocation().getDirection().normalize(),
				(double)distance.get(context),
				FluidCollisionMode.ALWAYS
		);
		
		// If we have a result, store it
		if(result != null) {
			context.setPosition(outVarPos, result.getHitPosition().toLocation(bukkitEntity.getWorld()));
		}
		
		// Return collision result
		return result != null;
	}
	
	@Override
	public Type getType(SpellExecutionContext context) throws SpellRuntimeError {
		return Type.BOOLEAN;
	}
	
	@PreviousToken(allowed = TokenType.METH_DISTANCE)
	public static RaytraceMethod generate(StreamTokens tokens) throws SyntaxError, BadTokenError {
		tokens.junkOrThrow(TokenType.PAREN_OPEN, "Function RAYTRACE needs a '(' after declaration.");
		Expression entity = Expression.generateExpression(tokens);
		tokens.junkOrThrow(TokenType.COMMA, "Function RAYTRACE needs a ',' after 1st argument.");
		String outvarPos = tokens.readVariableName();
		Expression distance = new DoubleExpression(DEFAULT_DISTANCE);
		if(tokens.isFutureConsume(TokenType.COMMA))
			distance = Expression.generateExpression(tokens);
		tokens.junkOrThrow(TokenType.PAREN_CLOSE, "Expected a ')' after last argument of RAYTRACE method.");
		return new RaytraceMethod(entity, outvarPos, distance);
	}
	
	@Override
	public String toString() {
		return "RAYCAST(" + entity + ", -> %" + outVarPos + ")";
	}
}
