package fr.shagolur.spells.parser.statements;

import fr.shagolur.core.entities.SEntity;
import fr.shagolur.spells.parser.elements.PreviousToken;
import fr.shagolur.spells.parser.elements.SpellExecutionContext;
import fr.shagolur.spells.parser.SpellParser;
import fr.shagolur.spells.parser.elements.TimeUnit;
import fr.shagolur.spells.parser.elements.TokenType;
import fr.shagolur.spells.parser.errors.BadTokenError;
import fr.shagolur.spells.parser.errors.SpellRuntimeError;
import fr.shagolur.spells.parser.errors.SyntaxError;
import fr.shagolur.spells.parser.elements.StreamTokens;

public class ApplyStatement extends Statement {
	
	private final int level;
	private final String effect, target;
	private final double secondsDuration;
	
	// APPLY <EFFECT> <LEVEL> DURING <DURATION> <UNIT> TO <TARGET>
	private ApplyStatement(String effect, int level, double duration, TimeUnit unit, String target) {
		this.effect = effect;
		this.level = level;
		this.target = target;
		secondsDuration = unit.toSeconds(duration);
	}
	
	@Override
	public void run(SpellExecutionContext context) throws SpellRuntimeError {
		SEntity entity = context.getEntity(target);
		entity.addEffect(effect, context.getCaster().getSEntity(), secondsDuration, 1);
	}
	
	@PreviousToken(allowed = TokenType.APPLY)
	public static ApplyStatement generate(StreamTokens tokens) throws BadTokenError, SyntaxError {
		String effect = tokens.readIdentifier();
		if( ! SpellParser.isEffectValid(effect))
			throw new SyntaxError("Invalid effect : \""+effect+"\".");
		int level = (int) tokens.readNumericValue();
		tokens.junkOrThrow(TokenType.DURING, "Expected a DURING after the target of a APPLY expression.");
		double duration = tokens.readNumericValue();
		String unitStr = tokens.readIdentifier();
		TimeUnit unit = TimeUnit.fromString(unitStr);
		if(unit == null)
			throw new SyntaxError("Unknown time unit : \""+unitStr+"\".");
		tokens.junkOrThrow(TokenType.TO, "Expected a TO after the target of a APPLY expression.");
		String targetVariable = tokens.readVariableName();
		tokens.junkOrThrow(TokenType.SEMICOLON, "Require a semicolon at the end of the APPLY expression.");
		return new ApplyStatement(effect, level, duration, unit, targetVariable);
	}
	
	@Override
	public String toString() {
		return "Apply{" + effect + " " + level + " during " + secondsDuration + "secs TO " + target + "}";
	}
}
