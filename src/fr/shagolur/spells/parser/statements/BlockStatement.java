package fr.shagolur.spells.parser.statements;

import fr.shagolur.spells.parser.elements.SpellExecutionContext;
import fr.shagolur.spells.parser.elements.PreviousToken;
import fr.shagolur.spells.parser.elements.StreamTokens;
import fr.shagolur.spells.parser.elements.TokenType;
import fr.shagolur.spells.parser.errors.BadTokenError;
import fr.shagolur.spells.parser.errors.SpellRuntimeError;
import fr.shagolur.spells.parser.errors.SyntaxError;

import java.util.ArrayList;
import java.util.List;

public class BlockStatement extends Statement {
	
	protected final List<Statement> content;
	
	public BlockStatement(List<Statement> content) {
		this.content = content;
	}
	
	@Override
	public void run(SpellExecutionContext context) throws SpellRuntimeError {
		SpellExecutionContext childrenContext = context.inherit();
		for(Statement statement : content)
			statement.run(childrenContext);
	}
	
	@PreviousToken(allowed = TokenType.BLOCK_START)
	public static List<Statement> generateBlock(StreamTokens tokens) throws BadTokenError, SyntaxError {
		List<Statement> block = new ArrayList<>();
		while(tokens.isNotFuture(TokenType.BLOCK_END)) {
			block.add(Statement.generateExpression(tokens));
		}
		tokens.junkOrThrow(TokenType.BLOCK_END, "Need a block end after a block.");
		return block;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("{\n");
		content.forEach(e -> sb.append("\t").append(e).append("\n"));
		return sb.append("}").toString();
	}
}
