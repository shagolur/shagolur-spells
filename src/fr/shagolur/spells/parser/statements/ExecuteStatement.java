package fr.shagolur.spells.parser.statements;

import fr.shagolur.core.Shagolur;
import fr.shagolur.spells.ShagolurSpells;
import fr.shagolur.spells.parser.elements.PreviousToken;
import fr.shagolur.spells.parser.elements.SpellExecutionContext;
import fr.shagolur.spells.parser.elements.StreamTokens;
import fr.shagolur.spells.parser.elements.TimeUnit;
import fr.shagolur.spells.parser.elements.TokenType;
import fr.shagolur.spells.parser.errors.BadTokenError;
import fr.shagolur.spells.parser.errors.SpellRuntimeError;
import fr.shagolur.spells.parser.errors.SyntaxError;
import fr.shagolur.spells.parser.expressions.Expression;
import fr.shagolur.spells.parser.expressions.Type;
import org.bukkit.Bukkit;

import java.util.List;

public class ExecuteStatement extends BlockStatement {
	
	private final TimeUnit unit;
	private final Expression time;
	
	ExecuteStatement(List<Statement> content, TimeUnit unit, Expression time) {
		super(content);
		this.unit = unit;
		this.time = time;
	}
	
	@Override
	public void run(SpellExecutionContext context) throws SpellRuntimeError {
		if(unit == null) {
			super.run(context.inherit());
			return;
		}
		
		if(time.getType(context) != Type.DOUBLE)
			throw new SpellRuntimeError("Bad type for expression " + time + ". Expected DOUBLE type for EXECUTE.");
		
		Bukkit.getScheduler().runTaskLater(
				ShagolurSpells.module(),
				() -> {
					try {
						super.run(context.inherit());
					} catch (SpellRuntimeError e) {
						Shagolur.error("Une erreur est survenue pendant l'éxécution d'un contexte en EXECUTE.");
						e.printStackTrace();
					}
				},
				unit.toSecondsTicks((double)time.get(context))
		);
	}
	
	@PreviousToken(allowed = TokenType.EXECUTE)
	public static ExecuteStatement generate(StreamTokens tokens) throws BadTokenError, SyntaxError {
		if(tokens.isFutureConsume(TokenType.BLOCK_START)) {
			List<Statement> content = BlockStatement.generateBlock(tokens);
			return new ExecuteStatement(content, null, null);
		}
		tokens.junkOrThrow(TokenType.AFTER, "A 'EXECUTE' must be followed either by '{' or 'AFTER <duration> <unit> {'.");
		Expression duration = Expression.generateExpression(tokens);
		String unitIdentifier = tokens.readIdentifier();
		TimeUnit unit = TimeUnit.fromString(unitIdentifier);
		if(unit == null)
			throw new SyntaxError("Unrecognized time unit : \"" + unitIdentifier + "\". in EXECUTE statement.");
		
		tokens.junkOrThrow(TokenType.BLOCK_START, "Expected a '{' after duration unit for EXECUTE.");
		List<Statement> content = BlockStatement.generateBlock(tokens);
		
		return new ExecuteStatement(content, unit, duration);
	}
}
