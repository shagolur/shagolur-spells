package fr.shagolur.spells.parser.statements;

import fr.shagolur.spells.parser.elements.SpellExecutionContext;
import fr.shagolur.spells.parser.elements.PreviousToken;
import fr.shagolur.spells.parser.elements.Selector;
import fr.shagolur.spells.parser.elements.StreamTokens;
import fr.shagolur.spells.parser.elements.TokenType;
import fr.shagolur.spells.parser.errors.BadTokenError;
import fr.shagolur.spells.parser.errors.SpellRuntimeError;
import fr.shagolur.spells.parser.errors.SyntaxError;

import java.util.List;

public class ForStatement extends BlockStatement {
	
	private final Selector selector;
	private final String variableName;
	private final String sourceEntity;
	private final double distance;
	
	// FOR <SELECTOR> AS <VAR> AROUND <ENTITY> IN <DISTANCE> DO
	private ForStatement(Selector selector, String variableName, String sourceEntity, double distance, List<Statement> content) {
		super(content);
		this.selector = selector;
		this.variableName = variableName;
		this.sourceEntity = sourceEntity;
		this.distance = distance;
	}
	
	@Override
	public void run(SpellExecutionContext context) throws SpellRuntimeError {
		//TODO
	}
	
	@PreviousToken(allowed = TokenType.FOR)
	public static ForStatement generate(StreamTokens tokens) throws BadTokenError, SyntaxError {
		String selectorStr = tokens.readIdentifier();
		Selector selector = Selector.fromString(selectorStr);
		if(selector == null)
			throw new SyntaxError("Unknown selector in FOR expression : \""+selectorStr+"\".");
		tokens.junkOrThrow(TokenType.AS, "Expected a AS after the selector in a FOR expression.");
		String varName = tokens.readVariableName();
		tokens.junkOrThrow(TokenType.AROUND, "Expected a AS after the selector in a FOR expression.");
		String sourceEntity = tokens.readVariableName();
		tokens.junkOrThrow(TokenType.IN, "Expected a IN after the entity source of a FOR expression.");
		double distance = tokens.readNumericValue();
		tokens.junkOrThrow(TokenType.DO, "Expected a DO after the distance of a FOR expression.");
		
		tokens.junkOrThrow(TokenType.BLOCK_START, "Expected a { after a DO in a FOR.");
		List<Statement> content = BlockStatement.generateBlock(tokens);
		
		return new ForStatement(selector, varName, sourceEntity, distance, content);
	}
	
	@Override
	public String toString() {
		return "FOR " + selector + " as " + variableName + " around " + sourceEntity + " in " + distance + " do " + super.toString();
	}
}
