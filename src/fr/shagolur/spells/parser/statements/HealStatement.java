package fr.shagolur.spells.parser.statements;

import fr.shagolur.core.entities.SEntity;
import fr.shagolur.spells.parser.elements.PreviousToken;
import fr.shagolur.spells.parser.elements.SpellExecutionContext;
import fr.shagolur.spells.parser.elements.TokenType;
import fr.shagolur.spells.parser.errors.BadTokenError;
import fr.shagolur.spells.parser.elements.StreamTokens;
import fr.shagolur.spells.parser.errors.SpellRuntimeError;
import fr.shagolur.spells.parser.errors.SyntaxError;
import fr.shagolur.spells.parser.expressions.Expression;
import fr.shagolur.spells.parser.expressions.Type;

public class HealStatement extends Statement {
	
	private final Expression amount;
	private final String variableTarget;
	
	// HEAL <TARGET> OF <AMOUNT>
	private HealStatement(String variableTarget, Expression amount) {
		this.variableTarget = variableTarget;
		this.amount = amount;
	}
	
	@Override
	public void run(SpellExecutionContext context) throws SpellRuntimeError {
		if(amount.getType(context) != Type.DOUBLE)
			throw new SpellRuntimeError("Bad type for expression " + amount + ". Expected DOUBLE type for HEAL.");
		SEntity target = context.getEntity(variableTarget);
		target.heal((double)amount.get(context));
	}
	
	@PreviousToken(allowed = TokenType.HEAL)
	public static HealStatement generate(StreamTokens tokens) throws SyntaxError, BadTokenError {
		String targetVariable = tokens.readVariableName();
		tokens.junkOrThrow(TokenType.OF, "Expected a OF after the target of a DAMAGE expression.");
		Expression amount = Expression.generateExpression(tokens);
		tokens.junkOrThrow(TokenType.SEMICOLON, "Require a semicolon at the end of the HEAL expression.");
		return new HealStatement(targetVariable, amount);
	}
	
	@Override
	public String toString() {
		return "Heal{" + variableTarget + " OF " + amount + "}";
	}
	
}
