package fr.shagolur.spells.parser.statements;

import fr.shagolur.core.common.Identifiable;
import fr.shagolur.core.entities.SEntity;
import fr.shagolur.spells.ShagolurSpells;
import fr.shagolur.spells.parser.elements.SpellExecutionContext;

import java.util.UUID;

public abstract class IdentifiableStatement extends Statement implements Identifiable {
	
	private final UUID uuidStatement = UUID.randomUUID();
	
	@Override
	public final UUID getUUID() {
		return uuidStatement;
	}
	
	public abstract int getMaxAmount();
	
	protected boolean canInvoke(SpellExecutionContext context) {
		return ShagolurSpells.getSpellsManager().getInvocations().canInvoke(this, context.getCaster());
	}
	protected void registerSpawn(SpellExecutionContext context, SEntity entity) {
		ShagolurSpells.getSpellsManager().getInvocations().spawn(this, context.getCaster(), entity);
	}
	protected void registerRemove(SpellExecutionContext context, SEntity entity) {
		ShagolurSpells.getSpellsManager().getInvocations().remove(this, context.getCaster(), entity);
	}

}
