package fr.shagolur.spells.parser.statements;

import fr.shagolur.spells.parser.elements.PreviousToken;
import fr.shagolur.spells.parser.elements.SpellExecutionContext;
import fr.shagolur.spells.parser.elements.StreamTokens;
import fr.shagolur.spells.parser.elements.TokenType;
import fr.shagolur.spells.parser.errors.BadTokenError;
import fr.shagolur.spells.parser.errors.SpellRuntimeError;
import fr.shagolur.spells.parser.errors.SyntaxError;
import fr.shagolur.spells.parser.expressions.Expression;
import fr.shagolur.spells.parser.expressions.Type;

import java.util.List;

public class IfStatement extends BlockStatement {
	
	private final Expression testExpression;
	private final BlockStatement elseBlock;
	
	// IF(<EXPRESSION>) DO
	private IfStatement(Expression testExpression, List<Statement> content, BlockStatement elseBlock) {
		super(content);
		this.testExpression = testExpression;
		this.elseBlock = elseBlock;
	}
	
	@Override
	public void run(SpellExecutionContext context) throws SpellRuntimeError {
		if(testExpression.getType(context) != Type.BOOLEAN)
			throw new SpellRuntimeError("Bad type for expression " + testExpression + ". Expected BOOLEAN type for IF.");
		// IF action
		if((boolean)testExpression.get(context))
			super.run(context);
		else if(elseBlock != null)
			elseBlock.run(context);
	}
	
	@PreviousToken(allowed = TokenType.IF)
	public static IfStatement generate(StreamTokens tokens) throws BadTokenError, SyntaxError {
		tokens.junkOrThrow(TokenType.PAREN_OPEN, "Expected a '(' after IF.");
		Expression test = Expression.generateExpression(tokens);
		tokens.junkOrThrow(TokenType.PAREN_CLOSE, "Expected a ')' after IF condition.");
		
		tokens.junkOrThrow(TokenType.BLOCK_START, "Expected a { after a IF condition.");
		List<Statement> content = BlockStatement.generateBlock(tokens);
		
		BlockStatement elseBlock = null;
		if(tokens.isFutureConsume(TokenType.ELSE)) {
			tokens.junkOrThrow(TokenType.BLOCK_START, "Expected a { after an ELSE.");
			elseBlock = new BlockStatement(BlockStatement.generateBlock(tokens));
		}
		
		return new IfStatement(test, content, elseBlock);
	}
	
	@Override
	public String toString() {
		return "IF( " + testExpression + ") " + super.toString() + (elseBlock==null?"":" ELSE " + elseBlock);
	}
}
