package fr.shagolur.spells.parser.statements;

import fr.shagolur.spells.parser.elements.SpellExecutionContext;
import fr.shagolur.spells.parser.elements.PreviousToken;
import fr.shagolur.spells.parser.elements.StreamTokens;
import fr.shagolur.spells.parser.elements.TokenType;
import fr.shagolur.spells.parser.errors.BadTokenError;
import fr.shagolur.spells.parser.errors.SpellRuntimeError;
import fr.shagolur.spells.parser.errors.SyntaxError;
import fr.shagolur.spells.parser.expressions.Expression;
import fr.shagolur.spells.parser.expressions.Type;

import java.util.List;

public class LoopStatement extends BlockStatement {
	
	private final String variableName;
	private final Expression count;
	
	// LOOP <N> AS <VAR> DO
	private LoopStatement(String variableName, Expression count, List<Statement> content) {
		super(content);
		this.variableName = variableName;
		this.count = count;
	}
	
	@Override
	public void run(SpellExecutionContext context) throws SpellRuntimeError {
		if(count.getType(context) != Type.DOUBLE)
			throw new SpellRuntimeError("Bad type for count expression " + count + ". Expected DOUBLE type for LOOP.");
		int countValue = (int) count.get(context);
		for(int i = 1; i <= countValue; i++) {
			context.setNumber(variableName, i);
			super.run(context);
		}
		context.remove(variableName);
	}
	
	@PreviousToken(allowed = TokenType.LOOP)
	public static LoopStatement generate(StreamTokens tokens) throws BadTokenError, SyntaxError {
		Expression count = Expression.generateExpression(tokens);
		tokens.junkOrThrow(TokenType.AS, "Expected a AS after the amount of time in a LOOP expression.");
		String varName = tokens.readVariableName();
		tokens.junkOrThrow(TokenType.DO, "Expected a DO after the variable of a LOOP expression.");
		
		tokens.junkOrThrow(TokenType.BLOCK_START, "Expected a { after a DO in a LOOP.");
		List<Statement> content = BlockStatement.generateBlock(tokens);
		
		return new LoopStatement(varName, count, content);
	}
	
	@Override
	public String toString() {
		return "LOOP " + count + " times, as " + variableName + " do " + super.toString();
	}
}
