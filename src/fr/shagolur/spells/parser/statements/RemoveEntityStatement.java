package fr.shagolur.spells.parser.statements;

import fr.shagolur.core.entities.SEntity;
import fr.shagolur.spells.parser.elements.PreviousToken;
import fr.shagolur.spells.parser.elements.SpellExecutionContext;
import fr.shagolur.spells.parser.elements.StreamTokens;
import fr.shagolur.spells.parser.elements.TokenType;
import fr.shagolur.spells.parser.errors.BadTokenError;
import fr.shagolur.spells.parser.errors.SpellRuntimeError;

public class RemoveEntityStatement extends Statement {
	
	private final String target;
	
	RemoveEntityStatement(String target) {
		this.target = target;
	}
	
	@Override
	public void run(SpellExecutionContext context) throws SpellRuntimeError {
		SEntity entity = context.getEntity(target);
		entity.die();
	}
	
	@PreviousToken(allowed = TokenType.REMOVE)
	public static RemoveEntityStatement generate(StreamTokens tokens) throws BadTokenError {
		tokens.junkOrThrow(TokenType.PAREN_OPEN, "REMOVE_ENTITY needs a '(' after declaration.");
		String target = tokens.readVariableName();
		tokens.junkOrThrow(TokenType.PAREN_CLOSE, "REMOVE_ENTITY needs a ')' after the target value.");
		return new RemoveEntityStatement(target);
	}
	
	@Override
	public String toString() {
		return "REMOVE_ENTITY{%"+target+"}";
	}
}
