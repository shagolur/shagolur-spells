package fr.shagolur.spells.parser.statements;

import fr.shagolur.core.entities.SEntity;
import fr.shagolur.core.players.SPlayer;
import fr.shagolur.spells.parser.elements.PreviousToken;
import fr.shagolur.spells.parser.elements.SpellExecutionContext;
import fr.shagolur.spells.parser.elements.TokenType;
import fr.shagolur.spells.parser.errors.BadTokenError;
import fr.shagolur.spells.parser.elements.StreamTokens;
import fr.shagolur.spells.parser.errors.SpellRuntimeError;

public class SendStatement extends Statement {
	
	private final String unformattedMessage, variableTarget;
	
	// SEND <STRING> TO <DEST>
	private SendStatement(String unformattedMessage, String variableTarget) {
		this.unformattedMessage = unformattedMessage;
		this.variableTarget = variableTarget;
	}
	
	@Override
	public void run(SpellExecutionContext context) throws SpellRuntimeError {
		SEntity entity = context.getEntity(variableTarget);
		if(entity instanceof SPlayer) {
			((SPlayer)entity).sendMessage(formatString(context, unformattedMessage));
		}
	}
	
	@PreviousToken(allowed = TokenType.SEND)
	public static SendStatement generate(StreamTokens tokens) throws BadTokenError {
		String msg = tokens.readStringValue();
		tokens.junkOrThrow(TokenType.TO, "Expected a TO after the message of a SEND expression.");
		String targetVariable = tokens.readVariableName();
		tokens.junkOrThrow(TokenType.SEMICOLON, "Require a semicolon at the end of the SEND expression.");
		return new SendStatement(msg, targetVariable);
	}
	
	@Override
	public String toString() {
		return "SEND{\"" + unformattedMessage + "\" TO " + variableTarget + "}";
	}
	
}
