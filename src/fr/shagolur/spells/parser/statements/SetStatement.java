package fr.shagolur.spells.parser.statements;

import fr.shagolur.core.Shagolur;
import fr.shagolur.core.entities.SEntity;
import fr.shagolur.spells.parser.elements.PreviousToken;
import fr.shagolur.spells.parser.elements.SpellExecutionContext;
import fr.shagolur.spells.parser.elements.StreamTokens;
import fr.shagolur.spells.parser.elements.TokenType;
import fr.shagolur.spells.parser.errors.BadTokenError;
import fr.shagolur.spells.parser.errors.SpellRuntimeError;
import fr.shagolur.spells.parser.errors.SyntaxError;
import fr.shagolur.spells.parser.expressions.Expression;
import org.bukkit.Location;

public class SetStatement extends Statement {
	
	private final String varName;
	private final Expression expression;
	
	private SetStatement(String varName, Expression expression) {
		this.varName = varName;
		this.expression = expression;
	}
	
	@Override
	public void run(SpellExecutionContext context) throws SpellRuntimeError {
		Object production = expression.get(context);
		switch(expression.getType(context)) {
			case DOUBLE -> context.setNumber(varName, (double) production);
			case LOCATION -> context.setPosition(varName, (Location) production);
			case BOOLEAN -> context.setBoolean(varName, (boolean) production);
			case ENTITY -> context.setEntity(varName, (SEntity) production);
		}
	}
	
	@PreviousToken(allowed = TokenType.SET)
	public static SetStatement generate(StreamTokens tokens) throws BadTokenError, SyntaxError {
		String varName = tokens.readIdentifier();
		tokens.junkOrThrow(TokenType.EQUAL, "Require a '=' after \"SET " +varName+"\" in a SET expression.");
		
		Expression expression = Expression.generateExpression(tokens);
		tokens.junkOrThrow(TokenType.SEMICOLON, "Require a semicolon at the end of the SET expression.");
		
		return new SetStatement(varName, expression);
	}
	
	@Override
	public String toString() {
		return "SET{" +varName + " := " + expression + "}";
	}
}
