package fr.shagolur.spells.parser.statements;

import fr.shagolur.core.Shagolur;
import fr.shagolur.core.entities.SEntity;
import fr.shagolur.spells.ShagolurSpells;
import fr.shagolur.spells.parser.elements.PreviousToken;
import fr.shagolur.spells.parser.elements.SpellExecutionContext;
import fr.shagolur.spells.parser.elements.TokenType;
import fr.shagolur.spells.parser.errors.BadTokenError;
import fr.shagolur.spells.parser.errors.SpellRuntimeError;
import fr.shagolur.spells.parser.errors.SyntaxError;
import fr.shagolur.spells.parser.data.DataStructure;
import fr.shagolur.spells.parser.elements.StreamTokens;
import fr.shagolur.spells.parser.expressions.Expression;
import fr.shagolur.spells.parser.expressions.Type;
import fr.shagolur.spells.spells.data.InvocationData;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;

public class SpawnStatement extends IdentifiableStatement {
	
	private final InvocationData invocation;
	private final Expression target;
	private final String optionalVar;
	
	// SPAWN <ENTITY_TYPE> (as <VAR>) at <POSITION> WITH {DATA}
	private SpawnStatement(EntityType entityType, String optionalVar, Expression target, DataStructure data) {
		this.target = target;
		this.optionalVar = optionalVar;
		this.invocation = new InvocationData(data, entityType);
	}
	
	@Override
	public void run(SpellExecutionContext context) throws SpellRuntimeError {
		if(target.getType(context) != Type.LOCATION)
			throw new SpellRuntimeError("Bad type for expression " + target + ". Expected LOCATION type for SPAWN.");
		if(!canInvoke(context)) {
			context.getCaster().sendMessageError("Impossible d'invoquer plus de " + getMaxAmount() + " entitées pour ce sort.");
			return;
		}
		Location loc = (Location) target.get(context);
		// Spawn the entity AMOUNT times
		for(int i = 0; i < invocation.getAmount(); i++) {
			// Spawn the entity
			SEntity entity = Shagolur.getEntitiesManager().spawn(invocation.asMonsterData(context), loc, context.getCaster().getSEntity());
			if(optionalVar != null)
				context.setEntity(optionalVar, entity);
			registerSpawn(context, entity);
			// Kill the entity after some time
			Bukkit.getScheduler().runTaskLater(ShagolurSpells.module(), () -> {
				if(! entity.isDead()) {
					registerRemove(context, entity);
					entity.die();
				}
			}, (long)(20*invocation.getDuration()));
		}
	}
	
	@PreviousToken(allowed = TokenType.SPAWN)
	public static SpawnStatement generate(StreamTokens tokens) throws BadTokenError, SyntaxError {
		String entityTypeString = tokens.readIdentifier();
		EntityType entityType;
		try {
			entityType = EntityType.valueOf(entityTypeString);
			if(entityType.getEntityClass() == null || !(LivingEntity.class.isAssignableFrom(entityType.getEntityClass()))) {
				throw new SyntaxError("This entity type exists but is invalid : \""+entityTypeString+"\"");
			}
		} catch(IllegalArgumentException e) {
			throw new SyntaxError("Unknown entity type : \""+entityTypeString+"\"");
		}
		String optionalVar = null;
		if(tokens.isFutureConsume(TokenType.AS)) {
			optionalVar = tokens.readVariableName();
		}
		
		tokens.junkOrThrow(TokenType.AT, "Expected a AT after the entity type of a SPAWN expression.");
		Expression target = Expression.generateExpression(tokens);
		tokens.junkOrThrow(TokenType.WITH, "Expected a WITCH after the position of a SPAWN expression.");
		
		tokens.junkOrThrow(TokenType.BLOCK_START, "Expected a { after a WITH in a SPAWN expression.");
		DataStructure dataStructure = DataStructure.generate(tokens);
		
		return new SpawnStatement(entityType, optionalVar, target, dataStructure);
	}
	
	@Override
	public int getMaxAmount() {
		return invocation.getLimit();
	}
	
	@Override
	public String toString() {
		return "Spawn{" + invocation.getEntityType() + " at " + target + " with " + invocation + "}";
	}
}
