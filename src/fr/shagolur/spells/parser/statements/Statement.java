package fr.shagolur.spells.parser.statements;

import fr.shagolur.core.Shagolur;
import fr.shagolur.core.entities.SEntity;
import fr.shagolur.core.players.SPlayer;
import fr.shagolur.core.utils.Utils;
import fr.shagolur.spells.ShagolurSpells;
import fr.shagolur.spells.parser.elements.SpellExecutionContext;
import fr.shagolur.spells.parser.elements.Selector;
import fr.shagolur.spells.parser.elements.Token;
import fr.shagolur.spells.parser.errors.BadTokenError;
import fr.shagolur.spells.parser.errors.SpellRuntimeError;
import fr.shagolur.spells.parser.errors.SyntaxError;
import fr.shagolur.spells.parser.elements.StreamTokens;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.FluidCollisionMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.RayTraceResult;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class Statement {
	
	private static final Pattern PATTERN_VARIABLE = Pattern.compile("%[A-Za-z_]+");
	
	public abstract void run(SpellExecutionContext context) throws SpellRuntimeError;
	
	public static Statement generateExpression(StreamTokens tokens) throws BadTokenError, SyntaxError {
		Token token = tokens.next();
		return switch (token.getType()) {
			// Simple statements
			case APPLY -> ApplyStatement.generate(tokens);
			case DAMAGE -> DamageStatement.generate(tokens);
			case HEAL -> HealStatement.generate(tokens);
			case SEND -> SendStatement.generate(tokens);
			case SET -> SetStatement.generate(tokens);
			case SPAWN -> SpawnStatement.generate(tokens);
			case THROW -> ThrowStatement.generate(tokens);
			case TELEPORT -> TeleportStatement.generate(tokens);
			case EXECUTE -> ExecuteStatement.generate(tokens);
			case REMOVE -> RemoveEntityStatement.generate(tokens);
			// blocks
			case FOR -> ForStatement.generate(tokens);
			case LOOP -> LoopStatement.generate(tokens);
			case IF -> IfStatement.generate(tokens);
			default -> throw new BadTokenError(token, "Unexpected token for start an expression.");
		};
	}
	
	protected List<Entity> getAround(Entity source, Selector selector, double distance) {
		return source.getLocation()
				.getWorld()
				.getEntities()
				.stream()
				.filter(e -> e.getLocation().distance(source.getLocation()) <= distance
						&& selector.accepts(source, e)
				)
				.toList();
	}
	
	/**
	 * Spawn particles around location.
	 */
	protected synchronized void spawnParticles(Location loc, Particle type, int count, double offsetHorizontal, double offsetVertical, double speed) {
		for(Player pl : getPlayersAround(loc, 100)) {
			spawnParticles(pl, loc, type, count, offsetHorizontal, offsetVertical, speed);
		}
	}
	
	/**
	 * Play sound around location.
	 */
	protected synchronized void playSound(Location loc, Sound sound, float volume, float pitch) {
		for(Player pl : getPlayersAround(loc, 100)) {
			pl.playSound(loc, sound, volume, pitch);
		}
	}
	
	/**
	 * Spawn particles around location.
	 */
	protected void spawnParticles(Player pl, Location loc, Particle type, int count, double offsetHorizontal, double offsetVertical, double speed) {
		pl.spawnParticle(type,
				loc.getX(), loc.getY(), loc.getZ(),
				count,
				offsetHorizontal, offsetVertical, offsetHorizontal,
				speed
		);
	}
	
	/**
	 * @return a List of {@link org.bukkit.entity.Player players} who are around a location.
	 * @see #getPlayersAroundPlayer(Player, double, boolean)
	 */
	protected synchronized List<Player> getPlayersAround(Location loc, double distance) {
		List<Player> list = new ArrayList<>();
		for(Player pl : loc.getWorld().getPlayers()) {
			if(pl.getLocation().distance(loc) <= distance)
				list.add(pl);
		}
		return list;
	}
	
	/**
	 * @return a List of {@link org.bukkit.entity.Player players} who are around a location.
	 * @param p source Player.
	 * @param distance maximal distance of the query.
	 * @param himself if true, the source player will be in the list.
	 * @see #getEntitiesAroundPlayer(Player, double, boolean)
	 */
	protected synchronized List<Player> getPlayersAroundPlayer(Player p, double distance, boolean himself) {
		List<Player> list = new ArrayList<>();
		for(Player pl : p.getWorld().getPlayers()) {
			if(pl.getUniqueId().equals(p.getUniqueId()) && (!himself))
				continue;
			if(pl.getLocation().distance(p.getLocation()) <= distance)
				list.add(pl);
		}
		return list;
	}
	
	/**
	 * @return a List of {@link org.bukkit.entity.Entity entities} who are around a location.
	 * @param p source Player.
	 * @param himself if true, the source player will be in the list.
	 * @param distance maximal distance of the query.
	 * @see #getPlayersAroundPlayer(Player, double, boolean)
	 */
	protected synchronized List<Entity> getEntitiesAroundPlayer(Player p, double distance, boolean himself) {
		List<Entity> list = new ArrayList<>();
		for(Entity e : p.getWorld().getEntities()) {
			if(e.getUniqueId().equals(p.getUniqueId()) && (!himself))
				continue;
			if(e.getLocation().distance(p.getLocation()) <= distance)
				list.add(e);
		}
		return list;
	}
	
	/**
	 * Apply particles to an entity during a time.
	 * If the entity is not valid, cancel the method.
	 * @param entity : Entity to follow with particles.
	 * @param duration : total duration of the particles (in ticks).
	 * @param frequency : frequency of the particles to be refreshed (in ticks).
	 * @param type type of the Particles. Caution : do not use particles who require more data.
	 * @param count amount of particles to be generated
	 * @param offset radius of the spawn of particles.
	 */
	protected void applyParticlesToEntity(Entity entity, int duration, int frequency, Particle type, int count, double offset) {
		new BukkitRunnable() {
			private int timeElapsed = 0;
			@Override
			public void run() {
				if( ! entity.isValid()) {
					cancel();
					return;
				}
				timeElapsed += frequency;
				spawnParticles(entity.getLocation(), type, count, offset, offset, .8);
				if(timeElapsed >= duration)
					cancel();
			}
		}.runTaskTimer(ShagolurSpells.module(), 0, frequency);
	}
	
	/**
	 * Remove an entity after a certain time, only if this entity is still valid.
	 * @param e entity to remove.
	 * @param seconds amount of seconds before removal.
	 */
	protected void scheduleRemoveEntity(Entity e, int seconds) {
		Bukkit.getScheduler().runTaskLater(ShagolurSpells.module(), () -> {
			if(e.isValid())
				e.remove();
		}, seconds*20L);
	}
	
	/**
	 * Use rayTraces to get the looked block of a player, to a certain distance.
	 * @param from Player who look.
	 * @param range maximum distance the Player can look at.
	 * @return null if no block was found.
	 */
	protected synchronized Block getLookedBlock(Player from, double range) {
		RayTraceResult targetBlockInfo = from.rayTraceBlocks(range, FluidCollisionMode.NEVER);
		if (targetBlockInfo == null) {
			from.sendMessage(Shagolur.prefix() + ChatColor.RED + "Ce sort n'a qu'une portée de " + ((int)range) + " blocs !");
			return null;
		}
		
		return targetBlockInfo.getHitBlock();
	}
	
	/**
	 * Change blocks temporarily.
	 * <br /> This is <b>client sided</b> so it DOES NOT modify your World.
	 * <br/>All players around 500 blocks of the first block of the map will be concerned. Stay in a same region.
	 * @param map Map of blocks to be changed.
	 * @param newType the Material to be display to all blocks.
	 * @param secondsDuration time during the one block will be in the 'new type' param state.
	 * @param tickModifier delta of seconds for the duration. Randomly set
	 */
	protected synchronized void changeBlocksTemporarily(Map<Location, Block> map, Material newType, int secondsDuration, int tickModifier) {
		if(map.isEmpty())
			return;
		final Map<Location, Block> blocks = new HashMap<>(map);
		final List<Player> players = getPlayersAround(blocks.keySet().iterator().next(), 500);
		for(final Location loc : blocks.keySet()) {
			new BukkitRunnable() {
				@Override
				public void run() {
					for(Player p : players) {
						p.sendBlockChange(loc, Bukkit.getServer().createBlockData(newType));
					}
				}
			}.runTaskLater(ShagolurSpells.module(), Utils.randInt(0, tickModifier));
		}
		
		for(final Location loc : blocks.keySet()) {
			new BukkitRunnable() {
				@Override
				public void run() {
					for(Player p : players) {
						p.sendBlockChange(loc, blocks.get(loc).getBlockData());
					}
				}
			}.runTaskLater(ShagolurSpells.module(), Utils.randInt((secondsDuration * 20) - tickModifier, (secondsDuration * 20) + tickModifier));
		}
	}
	
	/**
	 * Get all blocks between two points.
	 */
	protected List<Block> blocksFromTwoPoints(Location loc1, Location loc2) {
		List<Block> blocks = new ArrayList<>();
		
		int topBlockX = (Math.max(loc1.getBlockX(), loc2.getBlockX()));
		int bottomBlockX = (Math.min(loc1.getBlockX(), loc2.getBlockX()));
		int topBlockY = (Math.max(loc1.getBlockY(), loc2.getBlockY()));
		int bottomBlockY = (Math.min(loc1.getBlockY(), loc2.getBlockY()));
		int topBlockZ = (Math.max(loc1.getBlockZ(), loc2.getBlockZ()));
		int bottomBlockZ = (Math.min(loc1.getBlockZ(), loc2.getBlockZ()));
		
		for(int x = bottomBlockX; x <= topBlockX; x++) {
			for(int z = bottomBlockZ; z <= topBlockZ; z++) {
				for(int y = bottomBlockY; y <= topBlockY; y++) {
					Block block = loc1.getWorld().getBlockAt(x, y, z);
					blocks.add(block);
				}
			}
		}
		return blocks;
	}
	
	/**
	 * Get the closest Player of a point.
	 * @param loc Source location.
	 * @param range maximum range to query players.
	 * @return null if not Player has been found.
	 * @see #getPlayersAround(Location, double)
	 */
	protected SPlayer getClosestPlayerAtRange(Location loc, double range) {
		Player p = null;
		double dist = 10000;
		for(Player pl : loc.getWorld().getPlayers()) {
			double distance = pl.getLocation().distance(loc);
			if(distance < range && distance < dist) {
				p = pl;
				dist = distance;
			}
		}
		return p == null ? null : Shagolur.getPlayersManager().getPlayer(p);
	}
	
	protected SPlayer getClosestPlayerAtRange(SEntity source, double range) {
		Player p = null;
		double dist = 10000;
		for(Player pl : source.getLocation().getWorld().getPlayers()) {
			if(pl.getUniqueId().equals(source.getUUID()))
				continue;
			double distance = pl.getLocation().distance(source.getLocation());
			if(distance < range && distance < dist) {
				p = pl;
				dist = distance;
			}
		}
		return p == null ? null : Shagolur.getPlayersManager().getPlayer(p);
	}
	
	public static String formatString(SpellExecutionContext context, String message) {
		Matcher m = PATTERN_VARIABLE.matcher(message);
		while (m.find()) {
			String g = m.group();
			String varName = g.substring(1);
			message = message.replace(g, context.getAsString(varName));
		}
		return ChatColor.translateAlternateColorCodes('&', message);
	}
	
}
