package fr.shagolur.spells.parser.statements;

import fr.shagolur.spells.parser.elements.PreviousToken;
import fr.shagolur.spells.parser.elements.SpellExecutionContext;
import fr.shagolur.spells.parser.elements.StreamTokens;
import fr.shagolur.spells.parser.elements.TokenType;
import fr.shagolur.spells.parser.errors.BadTokenError;
import fr.shagolur.spells.parser.errors.SpellRuntimeError;
import fr.shagolur.spells.parser.errors.SpellTerminated;

public class StopStatement extends Statement {
	
	@Override
	public void run(SpellExecutionContext context) throws SpellRuntimeError {
		throw new SpellTerminated();
	}
	
	@PreviousToken(allowed = {TokenType.RETURN, TokenType.STOP})
	public static StopStatement generate(StreamTokens tokens) throws BadTokenError {
		tokens.junkOrThrow(TokenType.SEMICOLON, "Excepted a ';' after a STOP/RETURN.");
		return new StopStatement();
	}
}
