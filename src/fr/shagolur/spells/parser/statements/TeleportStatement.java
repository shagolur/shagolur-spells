package fr.shagolur.spells.parser.statements;

import fr.shagolur.core.entities.SEntity;
import fr.shagolur.spells.parser.elements.PreviousToken;
import fr.shagolur.spells.parser.elements.SpellExecutionContext;
import fr.shagolur.spells.parser.elements.StreamTokens;
import fr.shagolur.spells.parser.elements.TokenType;
import fr.shagolur.spells.parser.errors.BadTokenError;
import fr.shagolur.spells.parser.errors.SpellRuntimeError;
import fr.shagolur.spells.parser.errors.SyntaxError;
import fr.shagolur.spells.parser.expressions.Expression;
import fr.shagolur.spells.parser.expressions.Type;
import org.bukkit.Location;

public class TeleportStatement extends Statement {
	
	private final String variableEntity;
	private final Expression target;
	
	// TELEPORT <ENTITY> TO <LOC>
	private TeleportStatement(String variableEntity, Expression target) {
		this.variableEntity = variableEntity;
		this.target = target;
	}
	
	@Override
	public void run(SpellExecutionContext context) throws SpellRuntimeError {
		SEntity entity = context.getEntity(variableEntity);
		if(target.getType(context) == Type.ENTITY) {
			entity.teleport(((SEntity) target.get(context)).getLocation());
		}
		if(target.getType(context) != Type.LOCATION)
			throw new SpellRuntimeError("Bad type for expression " + target + ". Expected LOCATION OR ENTITY type for TELEPORT.");
		Location loc = (Location) target.get(context);
		entity.teleport(loc);
	}
	
	@PreviousToken(allowed = TokenType.TELEPORT)
	public static TeleportStatement generate(StreamTokens tokens) throws SyntaxError, BadTokenError {
		String entity = tokens.readVariableName();
		tokens.junkOrThrow(TokenType.TO, "Expected a TO after the message of a TELEPORT expression.");
		Expression target = Expression.generateExpression(tokens);
		tokens.junkOrThrow(TokenType.SEMICOLON, "Require a semicolon at the end of the TELEPORT expression.");
		return new TeleportStatement(entity, target);
	}
	
	@Override
	public String toString() {
		return "TELEPORT{\"" + variableEntity + "\" TO " + target + "}";
	}
	
}
