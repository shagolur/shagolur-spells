package fr.shagolur.spells.parser.statements;

import fr.shagolur.spells.parser.elements.ProjectileType;
import fr.shagolur.spells.parser.elements.StreamTokens;
import fr.shagolur.spells.parser.elements.SpellExecutionContext;
import fr.shagolur.spells.parser.data.DataStructure;
import fr.shagolur.spells.parser.elements.PreviousToken;
import fr.shagolur.spells.parser.elements.TokenType;
import fr.shagolur.spells.parser.errors.BadTokenError;
import fr.shagolur.spells.parser.errors.SpellRuntimeError;
import fr.shagolur.spells.parser.errors.SyntaxError;

public class ThrowStatement extends IdentifiableStatement {
	
	private final DataStructure data;
	private final String positionVariable;
	private final ProjectileType projectileType;
	
	// THROW [<TYPE>] FROM <POS> WITH {DATA}
	private ThrowStatement(ProjectileType projectileType, String positionVariable, DataStructure data) {
		this.projectileType = projectileType;
		this.positionVariable = positionVariable;
		this.data = data;
	}
	
	@Override
	public void run(SpellExecutionContext context) throws SpellRuntimeError {
		//TODO
	}
	
	@PreviousToken(allowed = TokenType.THROW)
	public static ThrowStatement generate(StreamTokens tokens) throws BadTokenError, SyntaxError {
		ProjectileType projectileType = ProjectileType.ENTITY;
		if(tokens.isFuture(TokenType.IDENTIFIER)) {
			String v = tokens.readIdentifier();
			projectileType = ProjectileType.fromString(v);
			if(projectileType == null)
				throw new SyntaxError("Unknown PROJECTILE type in THROW. Allowed : "+ProjectileType.allowedValues()+". Got : \""+v+"\".");
		}
		
		tokens.junkOrThrow(TokenType.FROM, "Expected a AT after the entity type of a SPAWN expression.");
		String positionVariable = tokens.readVariableName();
		tokens.junkOrThrow(TokenType.WITH, "Expected a WITCH after the position of a SPAWN expression.");
		
		tokens.junkOrThrow(TokenType.BLOCK_START, "Expected a { after a WITH in a SPAWN expression.");
		DataStructure dataStructure = DataStructure.generate(tokens);
		
		return new ThrowStatement(projectileType, positionVariable, dataStructure);
	}
	
	@Override
	public int getMaxAmount() {
		return 50;
	}
	
	@Override
	public String toString() {
		return "Spawn{" +projectileType + " at " + positionVariable + " with " + data + "}";
	}
}
