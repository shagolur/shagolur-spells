package fr.shagolur.spells.players;

import fr.shagolur.core.entities.SEntity;
import fr.shagolur.core.players.SPlayer;
import fr.shagolur.spells.spells.SpellCaster;
import org.jetbrains.annotations.NotNull;

public record CasterPlayer(SPlayer player) implements SpellCaster {
	
	public CasterPlayer(@NotNull SPlayer player) {
		this.player = player;
	}
	
	@Override
	public SEntity getSEntity() {
		return player;
	}
	
	@Override
	public void sendMessageInfo(String msg) {
		player.sendMessage(msg);
	}
	
	@Override
	public void sendMessageError(String msg) {
		player.sendError(msg);
	}
}
