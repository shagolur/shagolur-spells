package fr.shagolur.spells.spells;

import fr.shagolur.core.Shagolur;
import fr.shagolur.core.entities.SEntity;
import fr.shagolur.core.players.SPlayer;
import fr.shagolur.spells.parser.statements.IdentifiableStatement;
import org.jetbrains.annotations.Nullable;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

public class InvocationsManager {
	
	// statement -> spawn eneities
	private final Map<UUID, SpawnEntities> statementsData;
	
	InvocationsManager() {
		statementsData = new HashMap<>();
	}
	
	public boolean canInvoke(IdentifiableStatement statement, SpellCaster caster) {
		int current = getCurrentAmount(statement, caster);
		return statement.getMaxAmount() > current;
	}
	
	public int getCurrentAmount(IdentifiableStatement statement, SpellCaster caster) {
		return get(statement).getAmount(caster.getUUID());
	}
	
	private SpawnEntities get(IdentifiableStatement statement) {
		if(!statementsData.containsKey(statement.getUUID()))
			statementsData.put(statement.getUUID(), new SpawnEntities());
		return statementsData.get(statement.getUUID());
	}
	
	public void spawn(IdentifiableStatement statement, SpellCaster caster, SEntity entity) {
		get(statement).spawn(caster.getUUID(), entity);
	}
	
	public void remove(IdentifiableStatement statement, SpellCaster caster, SEntity entity) {
		get(statement).remove(caster.getUUID(), entity.getUUID());
	}
	
	public void clearPlayer(SPlayer player) {
		statementsData.values().forEach(se -> se.clear(player.getUUID()));
	}
	
	public void purgeAndClear() {
		statementsData.values().forEach(SpawnEntities::clearAll);
		statementsData.clear();
		Shagolur.log("Les invocations ont été purgées.");
	}
	
	private static class SpawnEntities {
		private final Map<UUID, Set<SEntity>> invocations = new HashMap<>();
		
		public Set<SEntity> getEntities(UUID casterUUID) {
			return invocations.getOrDefault(casterUUID, Collections.emptySet());
		}
		
		public @Nullable SEntity getEntity(UUID casterUUID, UUID entityUUID) {
			if(!invocations.containsKey(casterUUID))
				return null;
			return invocations.get(casterUUID).stream()
					.filter(in -> in.getUUID().equals(entityUUID))
					.findFirst()
					.orElse(null);
		}
		
		public void spawn(UUID casterUUID, SEntity entity) {
			if(!invocations.containsKey(casterUUID))
				invocations.put(casterUUID, new HashSet<>());
			invocations.get(casterUUID).add(entity);
		}
		
		public int getAmount(UUID casterUUID) {
			return getEntities(casterUUID).size();
		}
		
		public void clearAll() {
			invocations.keySet().forEach(this::clear);
		}
		
		public void clear(UUID casterUUID) {
			getEntities(casterUUID).forEach(SEntity::die);
			invocations.remove(casterUUID);
		}
		
		public void remove(UUID casterUUID, UUID entityUUID) {
			if(invocations.containsKey(casterUUID))
				invocations.get(casterUUID).removeIf(in -> in.getUUID().equals(entityUUID));
		}
		
	}
	
}
