package fr.shagolur.spells.spells;

import fr.shagolur.core.Shagolur;
import fr.shagolur.core.players.SPlayer;
import fr.shagolur.core.storage.Serializer;
import fr.shagolur.core.storage.Storable;
import fr.shagolur.core.ui.ItemSerializable;
import fr.shagolur.core.utils.ItemBuilder;
import fr.shagolur.spells.ShagolurSpells;
import fr.shagolur.spells.parser.SpellParser;
import fr.shagolur.spells.parser.elements.SpellExecutionContext;
import fr.shagolur.spells.parser.errors.BadTokenError;
import fr.shagolur.spells.parser.errors.SpellRuntimeError;
import fr.shagolur.spells.parser.errors.SpellTerminated;
import fr.shagolur.spells.parser.errors.SyntaxError;
import fr.shagolur.spells.parser.statements.Statement;
import fr.shagolur.spells.players.CasterPlayer;
import fr.shagolur.spells.storage.SpellSerializer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.persistence.PersistentDataType;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public class Spell implements Storable, ItemSerializable {
	
	public static NamespacedKey key() {
		return Shagolur.key("spell");
	}
	
	private final String id, name;
	private final double manaCost, cooldown;
	private List<Statement> parts;
	
	private final boolean valid;
	
	public Spell(SpellSerializer serializedSpell) {
		// Global data
		this.id = serializedSpell.id;
		this.name = ChatColor.translateAlternateColorCodes('&', serializedSpell.name);
		this.manaCost = serializedSpell.manaCost;
		this.cooldown = serializedSpell.cooldown;
		// Serialization
		try {
			this.parts = SpellParser.parse(serializedSpell.code);
		} catch(BadTokenError | SyntaxError e) {
			Shagolur.error(e.getClass().getSimpleName() + " during parsing of spell ["+id+"] : " + e.getMessage());
			valid = false;
			return;
		}
		valid = true;
	}
	
	public Spell(String id, String name, double mana, double cooldown, List<Statement> statements) {
		this.id = id;
		this.name = ChatColor.translateAlternateColorCodes('&', name);
		this.parts = statements;
		this.manaCost = mana;
		this.cooldown = cooldown;
		valid = true;
	}
	
	public boolean isValid() {
		return valid;
	}
	
	public void cast(SpellCaster caster) {
		SpellExecutionContext context = new SpellExecutionContext(caster);
		
		try {
			for(Statement expr : parts) {
				expr.run(context);
			}
		} catch(SpellTerminated ignored) {
			//We do nothing
		} catch(SpellRuntimeError e) {
			Shagolur.error("ERROR DURING SPELL(id="+id+",valid="+valid+") EXECUTION : " + e.getMessage());
		}
		
	}
	
	public String getName() {
		return name;
	}
	
	public double getManaCost() {
		return manaCost;
	}
	
	public double getCooldown() {
		return cooldown;
	}
	
	/**
	 * Get a list containing all the statements of the spell
	 * @return an unmodifiabe list of the statements.
	 */
	public List<Statement> getParts() {
		return Collections.unmodifiableList(parts);
	}
	
	private final Set<UUID> cooldownUuids = new HashSet<>();
	
	public void tryToCast(Player bukkitPlayer) {
		SPlayer player = Shagolur.getPlayersManager().getPlayer(bukkitPlayer);
		if(bukkitPlayer.getGameMode() == GameMode.CREATIVE) {
			cast(new CasterPlayer(player));
			return;
		}
		UUID uuid = bukkitPlayer.getUniqueId();
		if(cooldownUuids.contains(uuid)) {
			player.sendError("Impossible de relancer ce sort avant la fin du cooldown.");
			return;
		}
		if(!player.hasMana(manaCost)) {
			player.sendError("Tu n'as pas assez de mana pour lancer ce sort !");
			return;
		}
		// Consume mana & time
		player.consumeMana(manaCost);
		cooldownUuids.add(uuid);
		Bukkit.getScheduler().runTaskLater(
				ShagolurSpells.module(),
				() -> cooldownUuids.remove(uuid),
				(long)(20 * cooldown)
		);
		// Cast
		cast(new CasterPlayer(player));
	}
	
	public String getID() {
		return id;
	}
	
	@Override
	public Object storageID() {
		return id;
	}
	
	@Override
	public Serializer<?> serialize() {
		throw new RuntimeException("A spell CANNOT be serialized from minecraft plugin.");
	}
	
	@Override
	public boolean alreadyInBDD() {
		return true;
	}
	
	@Override
	public void declarePersistentInBDD() {}
	
	@Override
	public ItemStack toItemStack(int amount) {
		return new ItemBuilder(Material.PAPER, amount)
				.setDisplayName(name)
				.addLoreLine(ChatColor.GRAY + "Coût : " + ChatColor.AQUA + manaCost + " mana.")
				.addLoreLine(ChatColor.GRAY + "Cooldown : " + cooldown + " secondes.")
				.setPersistentData(key(), PersistentDataType.STRING, id)
				.build();
	}
	
}
