package fr.shagolur.spells.spells;

import fr.shagolur.core.common.Identifiable;
import fr.shagolur.core.entities.SEntity;

import java.util.UUID;

public interface SpellCaster extends Identifiable {

	SEntity getSEntity();
	
	@Override
	default UUID getUUID() {
		return getSEntity().getUUID();
	}
	
	void sendMessageInfo(String msg);
	void sendMessageError(String msg);

}
