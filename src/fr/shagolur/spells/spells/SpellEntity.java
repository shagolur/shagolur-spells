package fr.shagolur.spells.spells;

import fr.shagolur.core.entities.SEntity;
import fr.shagolur.core.entities.SEntityType;
import fr.shagolur.spells.ShagolurSpells;
import fr.shagolur.spells.parser.elements.Selector;
import fr.shagolur.spells.spells.data.ParticleData;
import fr.shagolur.spells.spells.data.SoundData;
import fr.shagolur.spells.spells.data.SpellEntityData;
import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.util.Collection;
import java.util.List;

public class SpellEntity extends SEntity {
	
	// Current position of the entity
	protected Location loc;
	// Current ovement direction (and speed !) of the entity
	protected Vector direction;
	
	protected final List<ParticleData> GFXs;
	protected final List<SoundData> SFXs;
	
	// Caster
	protected final SEntity caster;
	
	private double remainingDuration;
	
	private boolean doParticle, doSound;
	
	private final Selector selector;
	
	public SpellEntity(SpellEntityData data, Location loc, SEntity launcher, Selector selector) {
		super(SEntityType.SPELL);
		this.loc = loc.clone();
		this.caster = launcher;
		this.remainingDuration = data.getDuration();
		this.selector = selector;
		
		// Init other params
		this.direction = new Vector(0, 0, 0);
		GFXs = data.getGfxData();
		SFXs = data.getSfxData();
		//TODO speed et tout
		doParticle = false;
		doSound = false;
		
		// Add it to the manager
		ShagolurSpells.getSpellsManager().reportNewSpellEntity(this);
	}
	
	/**
	 * Set the direction of this SpellEntity.
	 *
	 * @param direction Vecor to go to.
	 */
	public void setDirection(Vector direction) {
		this.direction = direction;
	}
	
	@Override
	public void tick(double elapsed) {
		if(!exists())
			return;
		remainingDuration -= elapsed;
		// Moove
		loc.add(direction.multiply(elapsed));
		// Do GFX and SFX
		doGfxAndSfxEffects(elapsed);
		// APply real effects
		doRealEffects(elapsed);
		// Check if dead
		if(remainingDuration < 0) {
			die();
		}
	}
	
	public void addParticleEffect(ParticleData particleData) {
		doParticle = true;
		GFXs.add(particleData);
	}
	
	public void addSoundEffect(SoundData soundData) {
		doSound = true;
		SFXs.add(soundData);
	}
	
	protected void doRealEffects(double elapsed) {
		if (!exists())
			return;
		
	}
	
	private void doGfxAndSfxEffects(double elapsed) {
		//TODO tamper with elapsed
		if (!exists())
			return;
		for(Player pl : loc.getWorld().getNearbyPlayers(loc, 100)) {
			// GFX
			if (doParticle)
				GFXs.forEach(gfx -> gfx.play(pl));
			
			// SFX
			if (doSound)
				SFXs.forEach(gfx -> gfx.play(pl));
		}
	}
	
	protected Collection<Player> getPlayersAround(Location loc, double distance) {
		return loc.getWorld().getNearbyPlayers(
				loc,
				distance,
				pl -> selector.accepts(caster.getEntity(), pl)
		);
	}
	
	protected Collection<LivingEntity> getEntitiesAround(Location loc, double distance) {
		return loc.getWorld().getNearbyLivingEntities(
				loc,
				distance,
				pl -> selector.accepts(caster.getEntity(), pl)
		);
	}
	
	@Override
	public void die() {
		super.dead = true;
		//TODO d'autres trucs ?
		ShagolurSpells.getSpellsManager().reportRemovedSpellEntity(this);
	}
	
	/**
	 * Get the Spell's caster.
	 *
	 * @return the caster.
	 */
	public SEntity getCaster() {
		return caster;
	}
	
	/**
	 * Cancel an stop the SpellEntity.
	 */
	protected void cancel() {
		remainingDuration = 0;
		die();
	}
}
