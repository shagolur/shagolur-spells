package fr.shagolur.spells.spells;

import fr.shagolur.core.Shagolur;
import fr.shagolur.core.common.Purgeable;
import fr.shagolur.core.storage.PathProvider;
import fr.shagolur.core.storage.StorableManager;
import fr.shagolur.spells.ShagolurSpells;
import fr.shagolur.spells.parser.SpellParser;
import fr.shagolur.spells.parser.errors.BadTokenError;
import fr.shagolur.spells.parser.errors.SyntaxError;
import fr.shagolur.spells.parser.statements.Statement;
import fr.shagolur.spells.storage.SpellPathProvider;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;
import org.bukkit.persistence.PersistentDataType;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class SpellsManager extends StorableManager<Spell> implements Purgeable {
	
	private final Map<UUID, SpellEntity> spells = new HashMap<>();
	private final InvocationsManager invocations = new InvocationsManager();
	
	public InvocationsManager getInvocations() {
		return invocations;
	}
	
	public void reportNewSpellEntity(SpellEntity spellEntity) {
		spells.put(spellEntity.getUUID(), spellEntity);
	}
	
	public void reportRemovedSpellEntity(SpellEntity spellEntity) {
		spells.remove(spellEntity.getUUID());
	}
	
	public @Nullable Spell getFromItem(ItemStack is) {
		if(is == null || !is.hasItemMeta())
			return null;
		String id = is.getItemMeta().getPersistentDataContainer().get(Spell.key(), PersistentDataType.STRING);
		return get(id);
	}
	
	@Override
	public void purge() {
		spells.values().forEach(SpellEntity::cancel);
		spells.clear();
		invocations.purgeAndClear();
	}
	
	@Override
	protected PathProvider<Spell> getProvider() {
		return new SpellPathProvider();
	}
	
	@Override
	protected String getStoredTypeName() {
		return "spells";
	}
	
	@Override
	public void reloadFromBase() {
		super.reloadFromBase();
		int previous = collection.size();
		
		ShagolurSpells.module().saveDefaultConfig();
		ConfigurationSection config = ShagolurSpells.module().getConfig().getConfigurationSection("spells");
		if(config == null) {
			Shagolur.warning("[SPELLS] No configuration section detected for spells in config.");
			return;
		}
		for(String key : config.getKeys(false)) {
			ConfigurationSection section = config.getConfigurationSection(key);
			assert section != null;
			String name = section.getString("name");
			if(name == null) {
				Shagolur.error("[SPELLS] Spell ["+key+"] : invalid field \"name\".");
				continue;
			}
			String path = section.getString("file");
			if(path == null) {
				Shagolur.error("[SPELLS] Spell ["+key+"] : invalid field \"file\".");
				continue;
			}
			double mana = section.getDouble("mana");
			File file = new File(ShagolurSpells.module().getDataFolder(), path);
			if(!file.exists()) {
				Shagolur.error("[SPELLS] Spell ["+key+"] : could not find file \""+file.getAbsolutePath()+"\".");
				continue;
			}
			try {
				List<Statement> exp = SpellParser.parse(file);
				collection.add(new Spell(key, name, mana, exp));
			} catch (BadTokenError |SyntaxError e) {
				Shagolur.error("[SPELLS] Invalid spell ["+key+"], error when parsing "+path+" : " + e.getMessage());
			}
		}
		int size = collection.size();
		if(size > previous)
			Shagolur.log("[SPELLS] Loaded " + (size-previous) + " additional spells from files.");
	}
	
}
