package fr.shagolur.spells.spells.data;

import fr.shagolur.core.entities.LootTable;
import fr.shagolur.core.entities.MonsterData;
import fr.shagolur.core.entities.StatsHolder;
import fr.shagolur.spells.parser.data.DataStructure;
import fr.shagolur.spells.parser.elements.SpellExecutionContext;
import fr.shagolur.spells.parser.statements.Statement;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;

import java.util.Collections;
import java.util.List;

public class InvocationData implements StatsHolder {
	
	private final String displayName;
	private final double duration;
	private final double maxHealth, damages, speed, armorMagic, armorPhysical;
	private final boolean physical;
	private final int amount, limit;
	
	private final EntityType type;
	
	public InvocationData(DataStructure dataRaw, EntityType type) {
		this.type = type;
		displayName = dataRaw.getString("name", "Invocation de %caster");
		duration = dataRaw.getDouble("duration", 10);
		maxHealth = dataRaw.getDouble("health", 50);
		damages = dataRaw.getDouble("damages", 20);
		speed = dataRaw.getDouble("speed", 0.5);
		physical = dataRaw.getBoolean("physical", true);
		armorMagic = dataRaw.getDouble("magicalArmor", 2);
		armorPhysical = dataRaw.getDouble("physicalArmor", 2);
		amount = (int) dataRaw.getDouble("amount", 1);
		limit = (int) dataRaw.getDouble("limit", amount);
	}
	
	public double getDuration() {
		return duration;
	}
	
	@Override
	public double getMaxHealth() {
		return maxHealth;
	}
	
	@Override
	public double getMaxMana() {
		return 0;
	}
	
	@Override
	public double getPhysicalDamage() {
		return isPhysical() ? damages : 0;
	}
	
	@Override
	public double getMagicalDamage() {
		return isPhysical() ? 0 : damages;
	}
	
	@Override
	public double getPhysicalResistance() {
		return armorPhysical;
	}
	
	@Override
	public double getMagicalResistance() {
		return armorMagic;
	}
	
	public EntityType getEntityType() {
		return type;
	}
	
	@Override
	public double getLuck() {
		return 0;
	}
	
	@Override
	public int getLevel() {
		return 1;
	}
	
	@Override
	public StatsHolder clone() {
		try {
			return (StatsHolder) super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public double getSpeed() {
		return speed;
	}
	
	public boolean isPhysical() {
		return physical;
	}
	
	public MonsterData asMonsterData(SpellExecutionContext context) {
		String displayName = Statement.formatString(context, this.displayName);
		return new MonsterData(null, displayName, type, 0, this, new EmptyLoots(), Collections.emptyMap());
	}
	
	public int getAmount() {
		return amount;
	}
	
	public int getLimit() {
		return limit;
	}
	
	@Override
	public String toString() {
		return "InvocationData{'" + displayName + '\'' +
				", duration=" + duration +
				"s, maxHealth=" + maxHealth +
				", damages=" + damages +
				", speed=" + speed +
				", armorMagic=" + armorMagic +
				", armorPhysical=" + armorPhysical +
				", physical=" + physical +
				'}';
	}
	
	private static class EmptyLoots extends LootTable {
		public EmptyLoots() {
			super(Collections.emptyMap());
		}
		
		@Override
		public List<ItemStack> loot(double chance) {
			return Collections.emptyList();
		}
	}
}
