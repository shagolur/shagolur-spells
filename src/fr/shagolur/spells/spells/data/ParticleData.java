package fr.shagolur.spells.spells.data;

import fr.shagolur.spells.parser.data.DataStructure;
import fr.shagolur.spells.parser.errors.SyntaxError;
import org.bukkit.Particle;
import org.bukkit.entity.Player;

public class ParticleData {
	
	private final Particle particle;
	private final int count;
	private final double offsetX, offsetY, offsetZ;
	
	public ParticleData(DataStructure dataRaw) throws SyntaxError {
		String part = dataRaw.getString("particle");
		if(part == null)
			throw new SyntaxError("A ParticleData block must have a 'particle' string defined.");
		try {
			particle = Particle.valueOf(part);
		} catch(IllegalArgumentException e) {
			throw new SyntaxError("Unknown particle type for ParticleData block. {particle: \""+part+"\"}.");
		}
		count = (int) dataRaw.getDouble("count", 5);
		offsetX = dataRaw.getDouble("offsetX", 0.1);
		offsetY = dataRaw.getDouble("offsetY", 0.1);
		offsetZ = dataRaw.getDouble("offsetZ", 0.1);
	}
	
	public void play(Player p) {
		p.spawnParticle(particle, p.getLocation(), count, offsetX, offsetY, offsetZ);
	}
	
}
