package fr.shagolur.spells.spells.data;

import fr.shagolur.spells.parser.data.DataStructure;
import fr.shagolur.spells.parser.errors.SyntaxError;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class SoundData {
	
	private final Sound sound;
	private final float volume, pitch;
	
	public SoundData(DataStructure dataRaw) throws SyntaxError {
		String snd = dataRaw.getString("sound");
		if(snd == null)
			throw new SyntaxError("A SoundData block must have a 'sound' string defined.");
		try {
			sound = Sound.valueOf(snd);
		} catch(IllegalArgumentException e) {
			throw new SyntaxError("Unknown particle type for SoundData block. {sound: \""+snd+"\"}.");
		}
		volume = (float) dataRaw.getDouble("volume", 1);
		pitch = (float) dataRaw.getDouble("pitch", 1);
	}
	
	public void play(Player p) {
		p.playSound(p.getLocation(), sound,  volume, pitch);
	}
	
}
