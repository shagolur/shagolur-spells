package fr.shagolur.spells.spells.data;

import fr.shagolur.spells.parser.data.DataStructure;
import fr.shagolur.spells.parser.errors.SyntaxError;

import java.util.ArrayList;
import java.util.List;

public class SpellEntityData {
	
	private final double radius, speed, duration;
	private final double damagePerSecond;
	private final boolean piercing;
	
	private final List<SoundData> sfxData = new ArrayList<>();
	private final List<ParticleData> gfxData = new ArrayList<>();
	
	public SpellEntityData(DataStructure dataRaw) throws SyntaxError {
		radius = dataRaw.getDouble("radius", .1);
		speed = dataRaw.getDouble("speed", 0);
		duration = dataRaw.getDouble("duration", 3);
		damagePerSecond = dataRaw.getDouble("damages", 0);
		piercing = dataRaw.getBoolean("piercing", true);
		// Particles
		for(DataStructure ds : dataRaw.getDataStructureList("particles"))
			gfxData.add(new ParticleData(ds));
		DataStructure optionalGfx = dataRaw.getDataStructure("particle");
		if(optionalGfx != null)
			gfxData.add(new ParticleData(optionalGfx));
		// Sounds effects
		for(DataStructure ds : dataRaw.getDataStructureList("sounds"))
			sfxData.add(new SoundData(ds));
		DataStructure optionalSfx = dataRaw.getDataStructure("particle");
		if(optionalSfx != null)
			sfxData.add(new SoundData(optionalSfx));
	}
	
	public double getRadius() {
		return radius;
	}
	
	public double getSpeed() {
		return speed;
	}
	
	public double getDuration() {
		return duration;
	}
	
	public double getDamagePerSecond() {
		return damagePerSecond;
	}
	
	public boolean isPiercing() {
		return piercing;
	}
	
	public List<SoundData> getSfxData() {
		return sfxData;
	}
	
	public List<ParticleData> getGfxData() {
		return gfxData;
	}
}
