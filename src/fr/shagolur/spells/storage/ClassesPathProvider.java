package fr.shagolur.spells.storage;

import fr.shagolur.core.storage.PathProvider;
import fr.shagolur.core.storage.Serializer;
import fr.shagolur.spells.classes.SClass;

public class ClassesPathProvider implements PathProvider<SClass> {
	
	@Override
	public Class<SClass> getHandledClass() {
		return SClass.class;
	}
	
	@Override
	public Class<? extends Serializer<SClass>> getSerializerClass() {
		return SerializerSClass.class;
	}
	
	@Override
	public String getPath(Object... params) {
		if(params.length > 0)
			throw new RuntimeException("ClassesPathProvider only provides GET with 0 arguments !");
		return "class/classes/";
	}
}
