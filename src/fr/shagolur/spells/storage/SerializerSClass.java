package fr.shagolur.spells.storage;

import fr.shagolur.core.storage.Serializer;
import fr.shagolur.spells.classes.SClass;

public class SerializerSClass implements Serializer<SClass> {
	
	public String identifier;
	public String name;
	
	@Override
	public SClass deserialize() {
		return new SClass(identifier, name);
	}
}
