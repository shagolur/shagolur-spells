package fr.shagolur.spells.storage;

import fr.shagolur.core.storage.PathProvider;
import fr.shagolur.core.storage.Serializer;
import fr.shagolur.spells.spells.Spell;

public class SpellPathProvider implements PathProvider<Spell> {
	
	@Override
	public Class<Spell> getHandledClass() {
		return Spell.class;
	}
	
	@Override
	public Class<? extends Serializer<Spell>> getSerializerClass() {
		return SpellSerializer.class;
	}
	
	@Override
	public String getPath(Object... params) {
		if(params.length != 0)
			throw new RuntimeException("Only accepts NO arguments for spells path. Only GET allowed.");
		return "class/skills/";
	}
}
