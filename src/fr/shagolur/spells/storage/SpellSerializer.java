package fr.shagolur.spells.storage;

import com.google.gson.annotations.SerializedName;
import fr.shagolur.core.storage.Serializer;
import fr.shagolur.spells.spells.Spell;

public class SpellSerializer implements Serializer<Spell> {
	
	@SerializedName("identifier") public String id;
	public String name;
	@SerializedName("mana_cost") public double manaCost;
	public double cooldown;
	@SerializedName("skill_code") public String code;
	
	@Override
	public Spell deserialize() {
		return new Spell(this);
	}
}
