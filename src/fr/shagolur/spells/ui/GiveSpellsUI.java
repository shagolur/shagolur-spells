package fr.shagolur.spells.ui;

import fr.shagolur.core.ui.GiveListGUI;
import fr.shagolur.spells.ShagolurSpells;
import fr.shagolur.spells.spells.Spell;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.List;

public class GiveSpellsUI extends GiveListGUI<Spell> {
	
	public GiveSpellsUI(Player target) {
		super(ChatColor.BLUE + "" + ChatColor.BOLD + "Explorer les sorts disponibles", target);
	}
	
	@Override
	protected List<Spell> supply() {
		return ShagolurSpells.getSpellsManager().getStream().toList();
	}
}
